package pl.kamilczubaszek.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.kamilczubaszek.model.admissions.Record;

@Component
public class RecordValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Record.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Record record = (Record) o;
        if(record.getDay() == 0){
            errors.rejectValue("day", "recordForm.day");
        }
        if(record.getSubjectId() == 0){
            errors.rejectValue("subjectId", "recordForm.subjectId");
        }
    }
}
