package pl.kamilczubaszek.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.kamilczubaszek.model.account.User;
import pl.kamilczubaszek.model.account.UserEdit;

@Component
public class EditValidator implements Validator {
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }

    @Override
    public void validate(@Nullable Object o, Errors errors) {
        UserEdit userEdit = (UserEdit) o;

        if (!userEdit.getNewPassword().equals(userEdit.getPasswordConfirm())) {
            errors.rejectValue("passwordConfirm", "Diff.userForm.passwordConfirm");
        }
    }

    public void validatePassword(String userPass, String actualPass ,Errors errors) {

        if (!bCryptPasswordEncoder.matches(actualPass,userPass)) {
            errors.rejectValue("actualPassword", "incorrect.editForm.actualPassword");
        }
    }

}
