package pl.kamilczubaszek.tools.generator;

import java.io.InputStream;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRSaver;
import org.jfree.data.time.Day;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.kamilczubaszek.addons.Days;
import pl.kamilczubaszek.helpers.ColumnsGenerator;
import pl.kamilczubaszek.helpers.RecordHelper;
import pl.kamilczubaszek.model.admissions.Record;
import pl.kamilczubaszek.model.lessons.Plan;

@Component
public class ReportFiller {
    private String reportFileName;

    private JasperReport jasperReport;

    private JasperPrint jasperPrint;

    private Map<String, Object> parameters;

    private Plan plan;

    public ReportFiller() {
        parameters = new HashMap<>();
    }

    public void setPlan(Plan plan){
        this.plan = plan;
    }

    public void prepareReport() {
        compileReport();
        fillReport();
    }

    public void compileReport() {
        try {
            InputStream reportStream = getClass().getResourceAsStream("/".concat(reportFileName));
            jasperReport = JasperCompileManager.compileReport(reportStream);
            JRSaver.saveObject(jasperReport, reportFileName.replace(".jrxml", ".jasper"));
        } catch (JRException ex) {
            Logger.getLogger(ReportFiller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void fillReport() {
        try {
            ColumnsGenerator.generateDayRecords(plan);
            parameters = new HashMap<>();
            parameters.put("title",plan.getName());
            parameters.put("MondaySource",ColumnsGenerator.getMonday());
            parameters.put("TuesdaySource",ColumnsGenerator.getTuesday());
            parameters.put("WednesdaySource",ColumnsGenerator.getWednesday());
            parameters.put("ThursdaySource",ColumnsGenerator.getThursday());
            parameters.put("FridaySource",ColumnsGenerator.getFriday());
            parameters.put("SaturdaySource",ColumnsGenerator.getSaturday());
            parameters.put("SundaySource",ColumnsGenerator.getSunday());
            jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
        } catch (JRException ex) {
            Logger.getLogger(ReportFiller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    public String getReportFileName() {
        return reportFileName;
    }

    public void setReportFileName(String reportFileName) {
        this.reportFileName = reportFileName;
    }

    public JasperPrint getJasperPrint() {
        return jasperPrint;
    }

    /*private JRDataSource getDataSource() {
        Collection<Record> coll = plan.getRecords();
        return new JRBeanCollectionDataSource(coll);
    }*/
}
