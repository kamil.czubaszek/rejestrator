/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.kamilczubaszek.tools.generator;

import net.sf.jasperreports.engine.JRException;
import pl.kamilczubaszek.model.lessons.Plan;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *
 * @author kczubaszek
 */
public interface PlanGeneratorInterface {
    String generateTXT(Plan plan, HttpServletResponse response);
    void generatePDF(Plan plan, HttpServletResponse response) throws IOException, JRException;
}
