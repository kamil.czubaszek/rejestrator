package pl.kamilczubaszek.tools.generator;

import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.kamilczubaszek.addons.Days;
import pl.kamilczubaszek.helpers.StringHelper;
import pl.kamilczubaszek.model.admissions.Record;
import pl.kamilczubaszek.model.lessons.Plan;

import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author kczubaszek
 */
@Component
public class PlanGenerator implements PlanGeneratorInterface{
    @Autowired
    private ReportFiller reportFiller;

    @Override
    public String generateTXT(Plan plan, HttpServletResponse response) {
        String fileName = plan.getName()+".txt";
        response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
        Set<Record> records = plan.getRecords();
        records.stream().sorted(Comparator.comparing(Record::getDay).reversed());
        StringBuilder sb = new StringBuilder();
        long currentDay = 0;
        Iterator<Record> it = records.iterator();
        
        while (it.hasNext()) {
            Record record = it.next();
            if(currentDay != record.getDay()){
                sb.append(StringHelper.convertToUTF8(Days.fromInteger(record.getDay().intValue()).getName()));
                sb.append(System.getProperty("line.separator"));
                currentDay = record.getDay();
            }
            sb.append(record.getTime());
            sb.append(" - ");
            sb.append(record.getEndTime());
            sb.append(" ");
            sb.append(StringHelper.convertToUTF8(record.getSubject().getSubjectName()));
            sb.append(" ");
            sb.append(StringHelper.convertToUTF8(record.getSubject().getType().getName()));
            sb.append(" ");
            sb.append(StringHelper.convertToUTF8(record.getSubject().getTeacher().getTeacherName()));
            sb.append(System.getProperty("line.separator"));
        }
        return sb.toString();
    }

    @Override
    public void generatePDF(Plan plan, HttpServletResponse response) throws IOException, JRException {
        //Map<String,Object> params = new HashMap<>();
        //params.put("title",plan.getName());
        reportFiller.setReportFileName("main.jrxml");
        reportFiller.compileReport();
        //reportFiller.setParameters(params);
        reportFiller.setPlan(plan);
        reportFiller.fillReport();
        response.setContentType("application/x-pdf");
        response.setHeader("Content-disposition", "inline; filename="+plan.getName()+".pdf");

        final OutputStream outStream = response.getOutputStream();
        JasperExportManager.exportReportToPdfStream(reportFiller.getJasperPrint(), outStream);
    }
    
}
