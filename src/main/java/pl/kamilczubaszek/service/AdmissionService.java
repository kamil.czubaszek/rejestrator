package pl.kamilczubaszek.service;

import org.springframework.validation.Errors;
import pl.kamilczubaszek.model.account.User;
import pl.kamilczubaszek.model.admissions.Admission;
import pl.kamilczubaszek.model.admissions.helpers.JoinAdmissionHelper;
import pl.kamilczubaszek.model.lessons.Plan;

import java.util.List;

public interface AdmissionService {
    List<Admission> getAllTypes();
    void save(Admission admission);
    Admission findById(Long id);
    boolean joinToAdmission(JoinAdmissionHelper joinAdmissionHelper, Errors errors);
    void delete(Admission admission);
    boolean checkPlan(Plan plan);
}
