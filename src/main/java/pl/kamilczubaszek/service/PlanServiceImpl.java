package pl.kamilczubaszek.service;

import org.hibernate.EntityMode;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import pl.kamilczubaszek.model.account.User;
import pl.kamilczubaszek.model.admissions.Record;
import pl.kamilczubaszek.model.lessons.Plan;
import pl.kamilczubaszek.repository.PlanRepository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Set;

@Service
public class PlanServiceImpl implements PlanService {

    @Autowired
    PlanRepository planRepository;

    @Autowired
    AdmissionService admissionService;

    @Autowired
    UserService userService;

    @Autowired
    RecordService recordService;

    @Override
    public List<Plan> getAllPlans() {
        return planRepository.findAll();
    }

    @Override
    public void save(Plan plan) {
        planRepository.save(plan);
    }

    @Override
    public Plan getPlanById(Long id) {
        return planRepository.getOne(id);
    }

    @Override
    public void delete(Plan plan, User user) {
        if(!admissionService.checkPlan(plan)) {
            //Deleting plan from author account
            Set<Plan> plans = user.getPlans();
            plans.remove(plan);
            user.setPlans(plans);
            userService.update(user);

            //Deleting records from plan
            Set<Record> records = plan.getRecords();
            recordService.deleteAll(records);

            //Deleting plan
            plan.setUser(null);
            plan.getRecords().clear();
            plan.getUsers().clear();
            planRepository.delete(plan);
        }
    }
}
