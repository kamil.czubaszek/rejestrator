package pl.kamilczubaszek.service;

import pl.kamilczubaszek.model.admissions.Record;
import pl.kamilczubaszek.model.lessons.Subject;
import pl.kamilczubaszek.model.lessons.Type;

import java.util.List;
import java.util.Set;

public interface RecordService {
    List<Record> getAllRecords();
    void save(Record record);
    Record getRecordById(Long id);
    void delete(Record record);
    void deleteAll(Set<Record> records);
}
