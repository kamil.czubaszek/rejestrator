package pl.kamilczubaszek.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import pl.kamilczubaszek.model.account.User;
import pl.kamilczubaszek.model.account.UserEdit;
import pl.kamilczubaszek.model.account.UserEditMail;
import pl.kamilczubaszek.model.account.VerificationToken;
import pl.kamilczubaszek.repository.RoleRepository;
import pl.kamilczubaszek.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.kamilczubaszek.repository.VerificationTokenRepository;
import pl.kamilczubaszek.web.errors.UserAlreadyExistException;

import java.util.Calendar;
import java.util.HashSet;
import pl.kamilczubaszek.validator.PeselValidator;

@Service
public class UserServiceImpl implements UserService {
    public static final String TOKEN_INVALID = "invalidToken";
    public static final String TOKEN_EXPIRED = "expired";
    public static final String TOKEN_VALID = "valid";
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private VerificationTokenRepository tokenRepository;

    @Override
    public void save(User user) {
        if (emailExist(user.getEmail())) {
            throw new UserAlreadyExistException("There is an account with that email adress: " + user.getEmail());
        }
        PeselValidator peselValidator = new PeselValidator(user.getPesel());
        user.setSex(peselValidator.getSex());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-MM-yyyy");
        String date = peselValidator.getFormattedData(peselValidator.getBirthDay()) + "-" + peselValidator.getFormattedData(peselValidator.getBirthMonth()) + "-" + peselValidator.getFormattedData(peselValidator.getBirthYear());
        LocalDate dateOfBirth = LocalDate.parse(date, formatter);
        user.setDateOfBirth(dateOfBirth);
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRoles(new HashSet<>(roleRepository.findAll()));
        userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User findById(Long id) {
        return userRepository.findById(id).get();
    }

    @Override
    public User getUser(String verificationToken) {
        final VerificationToken token = tokenRepository.findByToken(verificationToken);
        if (token != null) {
            return token.getUser();
        }
        return null;
    }

    @Override
    public void changePassword(UserEdit userEdit, Long id) {
        User user = userRepository.getOne(id);
        user.setPassword(bCryptPasswordEncoder.encode(userEdit.getNewPassword()));
        userRepository.save(user);
    }

    @Override
    public void changeMail(UserEditMail userEdit, Long id) {
        User user = userRepository.getOne(id);
        user.setEmail(userEdit.getNewMail());
        userRepository.save(user);
    }

    @Override
    public boolean emailExist(String email) {
        return userRepository.findByEmail(email) != null;
    }

    @Override
    public void createVerificationTokenForUser(User user, String token) {
        final VerificationToken myToken = new VerificationToken(token, user);
        tokenRepository.save(myToken);
    }

    @Override
    public String validateVerificationToken(String token) {
        final VerificationToken verificationToken = tokenRepository.findByToken(token);
        if (verificationToken == null) {
            return TOKEN_INVALID;
        }

        final User user = verificationToken.getUser();
        final Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            tokenRepository.delete(verificationToken);
            return TOKEN_EXPIRED;
        }

        user.setEnabled(true);
        userRepository.save(user);
        return TOKEN_VALID;
    }

    @Override
    public User getAuthUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)authentication.getPrincipal();
        return user;
    }

    @Override
    public void update(User user) {
        userRepository.save(user);
    }
}
