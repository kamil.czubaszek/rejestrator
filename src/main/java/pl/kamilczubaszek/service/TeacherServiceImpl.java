package pl.kamilczubaszek.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kamilczubaszek.model.lessons.Teacher;
import pl.kamilczubaszek.repository.TeacherRepository;

import java.util.List;

@Service
class TeacherServiceImpl implements TeacherService{

    @Autowired
    private TeacherRepository teacherRepository;

    @Override
    public List<Teacher> getAllTeachers() {
        return teacherRepository.findAll();
    }

    @Override
    public void save(Teacher teacher) {
        teacherRepository.save(teacher);
    }

    @Override
    public Teacher getTeacherById(Long id) {
        return teacherRepository.getOne(id);
    }

    @Override
    public void delete(Teacher teacher) {
        teacher.setUser(null);
        teacherRepository.delete(teacher);
    }
}
