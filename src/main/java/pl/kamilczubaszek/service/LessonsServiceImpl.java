package pl.kamilczubaszek.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kamilczubaszek.model.lessons.Type;
import pl.kamilczubaszek.repository.LessonsTypeRepository;

import java.util.List;

@Service
public class LessonsServiceImpl implements LessonsService {

    @Autowired
    private LessonsTypeRepository lessonsTypeRepository;

    @Autowired
    private SubjectService subjectService;

    @Override
    public List<Type> getAllTypes() {
        return lessonsTypeRepository.findAll();
    }

    @Override
    public void save(Type type) {
        lessonsTypeRepository.save(type);
    }

    @Override
    public Type getTypeById(Long id) {
        return lessonsTypeRepository.getOne(id);
    }

    @Override
    public void delete(Type type) {
        if(!subjectService.checkType(type)) {
            type.setUser(null);
            lessonsTypeRepository.delete(type);
        }
    }
}
