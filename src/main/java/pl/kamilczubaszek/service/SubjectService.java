package pl.kamilczubaszek.service;

import pl.kamilczubaszek.model.lessons.Subject;
import pl.kamilczubaszek.model.lessons.Type;

import java.util.List;

public interface SubjectService {
    List<Subject> getAllSubjects();
    void save(Subject subject);
    Subject getSubjectById(Long id);
    void delete(Subject subject);
    boolean checkType(Type type);
}
