package pl.kamilczubaszek.service;

import pl.kamilczubaszek.model.lessons.Teacher;

import java.util.List;

public interface TeacherService {
    List<Teacher> getAllTeachers();
    void save(Teacher teacher);
    Teacher getTeacherById(Long id);
    void delete(Teacher teacher);
}
