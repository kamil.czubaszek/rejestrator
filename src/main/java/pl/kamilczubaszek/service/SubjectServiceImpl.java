package pl.kamilczubaszek.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kamilczubaszek.model.account.User;
import pl.kamilczubaszek.model.lessons.Subject;
import pl.kamilczubaszek.model.lessons.Type;
import pl.kamilczubaszek.repository.SubjectRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class SubjectServiceImpl implements SubjectService {
    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private UserService userService;

    @Override
    public List<Subject> getAllSubjects() {
        return subjectRepository.findAll();
    }

    @Override
    public void save(Subject subject) {
        subjectRepository.save(subject);
    }

    @Override
    public Subject getSubjectById(Long id) {
        return subjectRepository.getOne(id);
    }

    @Override
    public void delete(Subject subject) {
        subject.getUser().getSubjects().remove(subject);
        subject.setUser(null);
        subject.setType(null);
        subject.setTeacher(null);
        subjectRepository.delete(subject);
    }

    @Override
    public boolean checkType(Type type) {
        User user = userService.findById(userService.getAuthUser().getId());
        Set<Type> user_types = user.getTypes();
        Set<Subject> user_subjects = user.getSubjects();

        for(Subject subject : user_subjects){
            if(user_types.contains(type) && type.getId() == subject.getType().getId()){
                return true;
            }
        }
        return false;
    }
}
