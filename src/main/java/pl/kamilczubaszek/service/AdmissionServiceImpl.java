package pl.kamilczubaszek.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import pl.kamilczubaszek.model.account.User;
import pl.kamilczubaszek.model.admissions.Admission;
import pl.kamilczubaszek.model.admissions.helpers.JoinAdmissionHelper;
import pl.kamilczubaszek.model.lessons.Plan;
import pl.kamilczubaszek.repository.AdmissionRepository;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Service
public class AdmissionServiceImpl implements AdmissionService {

    @Autowired
    private AdmissionRepository admissionRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private PlanService planService;

    @Override
    public List<Admission> getAllTypes() {
        return admissionRepository.findAll();
    }

    @Override
    public void save(Admission admission) {
        admission.setPassword(bCryptPasswordEncoder.encode(admission.getPassword()));
        admissionRepository.save(admission);
    }

    @Override
    public Admission findById(Long id) {
        return admissionRepository.findById(id).get();
    }

    @Override
    public boolean joinToAdmission(JoinAdmissionHelper joinAdmissionHelper, Errors errors) {
        Admission admission = admissionRepository.findById(joinAdmissionHelper.getId()).orElse(null);
        if(admission!=null){
            if(bCryptPasswordEncoder.matches(joinAdmissionHelper.getPassword(),admission.getPassword())){
                User user = userService.findById(userService.getAuthUser().getId());
                Set<Admission> otherAdmissions = user.getOtherAdmissions();
                Set<User> members = admission.getMembers();
                if(containsID(otherAdmissions,admission.getId())){
                    errors.rejectValue("id","incorrect.admission.idUsed");
                    return false;
                }
                members.add(user);
                admission.setMembers(members);
                otherAdmissions.add(admission);
                user.setOtherAdmissions(otherAdmissions);

                /* add plan for user */
                Plan plan = admission.getPlan();
                Set<User> user_plan = plan.getUsers();
                Set<Plan> plan_user = user.getOtherPlans();
                user_plan.add(user);
                plan_user.add(plan);
                plan.setUsers(user_plan);
                user.setOtherPlans(plan_user);

                admissionRepository.save(admission);
                userService.update(user);
                return true;
            }
            else{
                errors.rejectValue("password","incorrect.admission.password");
                return false;
            }
        }
        else{
            errors.rejectValue("id","incorrect.admission.id");
            return false;
        }
    }

    @Override
    public void delete(Admission admission) {
        admissionRepository.delete(admission);
    }

    @Override
    public boolean checkPlan(Plan plan) {
        List<Admission> list = admissionRepository.findAll();
        return list.stream().anyMatch(o -> o.getPlan().getId() == plan.getId());
    }

    public boolean containsID(final Set<Admission> list, final Long id){
        return list.stream().anyMatch(o -> o.getId() == id);
    }
}
