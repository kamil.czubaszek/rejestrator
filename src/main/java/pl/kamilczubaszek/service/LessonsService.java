package pl.kamilczubaszek.service;

import pl.kamilczubaszek.model.lessons.Type;

import java.util.List;

public interface LessonsService {
    List<Type> getAllTypes();
    void save(Type type);
    Type getTypeById(Long id);
    void delete(Type type);
}
