package pl.kamilczubaszek.service;

import pl.kamilczubaszek.model.account.User;
import pl.kamilczubaszek.model.lessons.Plan;

import java.util.List;

public interface PlanService {
    List<Plan> getAllPlans();
    void save(Plan plan);
    Plan getPlanById(Long id);
    void delete(Plan plan, User user);
}
