package pl.kamilczubaszek.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kamilczubaszek.model.account.User;
import pl.kamilczubaszek.model.admissions.Record;
import pl.kamilczubaszek.model.lessons.Subject;
import pl.kamilczubaszek.model.lessons.Type;
import pl.kamilczubaszek.repository.RecordRepository;

import java.util.List;
import java.util.Set;

@Service
public class RecordServiceImpl implements RecordService {

    @Autowired
    private RecordRepository recordRepository;

    @Autowired
    private UserService userService;

    @Override
    public List<Record> getAllRecords() {
        return recordRepository.findAll();
    }

    @Override
    public void save(Record record) {
        recordRepository.save(record);
    }

    @Override
    public Record getRecordById(Long id) {
        return recordRepository.getOne(id);
    }

    @Override
    public void delete(Record record) {
        record.setSubject(null);
        record.setPlan(null);
        recordRepository.delete(record);
    }

    @Override
    public void deleteAll(Set<Record> records) {
        for (Record record : records){
            delete(record);
        }
    }
}
