package pl.kamilczubaszek.service;

import org.hibernate.Hibernate;
import pl.kamilczubaszek.model.account.User;
import pl.kamilczubaszek.model.account.UserEdit;
import pl.kamilczubaszek.model.account.UserEditMail;

public interface UserService {
    void save(User user);

    User findByUsername(String username);

    User findById(Long id);

    User getUser(String verificationToken);

    void changePassword(UserEdit userEdit, Long id);

    void changeMail(UserEditMail userEdit, Long id);

    boolean emailExist(final String email);

    void createVerificationTokenForUser(User user, String token);

    String validateVerificationToken(String token);

    User getAuthUser();

    void update(User user);
}
