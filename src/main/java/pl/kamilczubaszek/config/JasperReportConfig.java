package pl.kamilczubaszek.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.kamilczubaszek.tools.generator.ReportExporter;
import pl.kamilczubaszek.tools.generator.ReportFiller;

@Configuration
public class JasperReportConfig {
    @Bean
    public ReportFiller reportFiller() {
        return new ReportFiller();
    }

    @Bean
    public ReportExporter reportExporter() {
        return new ReportExporter();
    }
}
