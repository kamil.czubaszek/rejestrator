package pl.kamilczubaszek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.kamilczubaszek.model.admissions.Record;
import pl.kamilczubaszek.model.lessons.Subject;

@Repository
public interface RecordRepository extends JpaRepository<Record, Long> {
    /*@Modifying
    @Query("update Record r set r.subject = ?1 where r.id = ?2")
    void update(Subject subject, long recordID);*/
}
