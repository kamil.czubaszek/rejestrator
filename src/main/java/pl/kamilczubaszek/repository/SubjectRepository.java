package pl.kamilczubaszek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.kamilczubaszek.model.lessons.Subject;

public interface SubjectRepository extends JpaRepository<Subject, Long> {
}
