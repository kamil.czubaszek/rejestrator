package pl.kamilczubaszek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.kamilczubaszek.model.lessons.Plan;
import pl.kamilczubaszek.model.lessons.Teacher;

@Repository
public interface PlanRepository extends JpaRepository<Plan, Long> {
}
