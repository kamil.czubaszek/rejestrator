package pl.kamilczubaszek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kamilczubaszek.model.lessons.Type;


@Repository
public interface LessonsTypeRepository extends JpaRepository<Type, Long> {
}
