package pl.kamilczubaszek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kamilczubaszek.model.lessons.Teacher;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Long> {
}
