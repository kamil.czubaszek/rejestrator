package pl.kamilczubaszek.repository;

import org.springframework.stereotype.Repository;
import pl.kamilczubaszek.model.account.Role;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{
}
