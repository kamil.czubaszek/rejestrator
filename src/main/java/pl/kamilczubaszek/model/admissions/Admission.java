package pl.kamilczubaszek.model.admissions;

import org.hibernate.annotations.Type;
import org.springframework.context.annotation.Scope;
import org.springframework.format.annotation.DateTimeFormat;
import pl.kamilczubaszek.model.account.User;
import pl.kamilczubaszek.model.lessons.Plan;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "admission")
public class Admission {

    @Id
    @Column(name="admission_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="name")
    @NotNull
    private String name;

    @NotNull
    private String password;

    @Column(name="start_date")
    @Type(type="date")
    @Temporal(TemporalType.DATE)
    private Date startDate;

    @Transient
    private String startDateTemp;

    @Column(name="start_time")
    @NotNull
    @DateTimeFormat(pattern = "HH:mm")
    private LocalTime startTime;

    @Column(name="active")
    private boolean active;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinTable(name = "user_admissions", joinColumns = @JoinColumn(name = "admission_id"), inverseJoinColumns = @JoinColumn(name = "user_id"))
    private User user;

    @ManyToMany(mappedBy = "otherAdmissions")
    private Set<User> members;

    @OneToOne(fetch = FetchType.LAZY)
    private Plan plan;

    @Transient
    private Long planID;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getStartDateTemp() {
        return startDateTemp;
    }

    public void setStartDateTemp(String startDateTemp) {
        this.startDateTemp = startDateTemp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<User> getMembers() {
        return members;
    }

    public void setMembers(Set<User> members) {
        this.members = members;
    }

    public Plan getPlan() {
        return plan;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }

    public Long getPlanID() {
        return planID;
    }

    public void setPlanID(Long planID) {
        this.planID = planID;
    }
}
