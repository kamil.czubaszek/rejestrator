package pl.kamilczubaszek.model.admissions.helpers;

import javax.validation.constraints.NotNull;

public class JoinAdmissionHelper {
    @NotNull
    private Long id;
    @NotNull
    private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
