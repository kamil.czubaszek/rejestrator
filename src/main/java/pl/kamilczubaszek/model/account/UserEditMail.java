package pl.kamilczubaszek.model.account;

import org.hibernate.validator.constraints.Length;

public class UserEditMail {
    @Length(min = 1,message = "Pole nie może być puste")
    private String actualPassword;
    private String actualMail;
    @Length(min = 1,message = "Pole nie może być puste")
    private String newMail;

    public String getActualPassword() {
        return actualPassword;
    }

    public void setActualPassword(String actualPassword) {
        this.actualPassword = actualPassword;
    }

    public String getActualMail() {
        return actualMail;
    }

    public void setActualMail(String actualMail) {
        this.actualMail = actualMail;
    }

    public String getNewMail() {
        return newMail;
    }

    public void setNewMail(String newMail) {
        this.newMail = newMail;
    }
}
