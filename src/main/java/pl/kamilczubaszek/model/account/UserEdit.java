package pl.kamilczubaszek.model.account;

import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserEdit {
    @Length(min = 1,message = "Pole nie może być puste")
    private String actualPassword;
    @Size(min=6,max=20, message="Hasło musi zawierać od 6 do 20 znaków")
    private String newPassword;
    @Length(min = 1,message = "Pole nie może być puste")
    private String passwordConfirm;

    public String getActualPassword() {
        return actualPassword;
    }

    public void setActualPassword(String actualPassword) {
        this.actualPassword = actualPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}
