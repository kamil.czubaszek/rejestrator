package pl.kamilczubaszek.model.lessons;

import org.springframework.format.annotation.DateTimeFormat;
import pl.kamilczubaszek.model.account.User;
import pl.kamilczubaszek.model.admissions.Record;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;
import java.util.Date;
import java.util.Set;

@Entity
public class Plan {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private String name;

    @Column(name = "records")
    @OneToMany(mappedBy="plan")
    private Set<Record> records;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinTable(name = "user_plans", joinColumns = @JoinColumn(name = "plan_id"), inverseJoinColumns = @JoinColumn(name = "user_id"))
    private User user;

    @ManyToMany(mappedBy = "otherPlans")
    private Set<User> users;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Record> getRecords() {
        return records;
    }

    public void setRecords(Set<Record> records) {
        this.records = records;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
}
