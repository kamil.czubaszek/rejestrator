package pl.kamilczubaszek.helpers;

import pl.kamilczubaszek.model.admissions.Record;
import pl.kamilczubaszek.model.lessons.Plan;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ColumnsGenerator {
    private static List<Record> monday;
    private static List<Record> tuesday;
    private static List<Record> wednesday;
    private static List<Record> thursday;
    private static List<Record> friday;
    private static List<Record> saturday;
    private static List<Record> sunday;

    public static void generateDayRecords(Plan plan){
        Set<Record> records = plan.getRecords();
        monday = records.stream().filter(item->item.getDay() == 1).collect(Collectors.toList());
        tuesday = records.stream().filter(item->item.getDay() == 2).collect(Collectors.toList());
        wednesday = records.stream().filter(item->item.getDay() == 3).collect(Collectors.toList());
        thursday = records.stream().filter(item->item.getDay() == 4).collect(Collectors.toList());
        friday = records.stream().filter(item->item.getDay() == 5).collect(Collectors.toList());
        saturday = records.stream().filter(item->item.getDay() == 6).collect(Collectors.toList());
        sunday = records.stream().filter(item->item.getDay() == 7).collect(Collectors.toList());

        List<Integer> sizes = new ArrayList<>();
        sizes.add(monday.size());
        sizes.add(tuesday.size());
        sizes.add(wednesday.size());
        sizes.add(thursday.size());
        sizes.add(friday.size());
        sizes.add(saturday.size());
        sizes.add(sunday.size());

        int maxSize = sizes.stream().collect(Collectors.summarizingInt(Integer::intValue)).getMax();

        while(monday.size() < maxSize)
            monday.add(null);
        while(tuesday.size() < maxSize)
            tuesday.add(null);
        while(wednesday.size() < maxSize)
            wednesday.add(null);
        while(thursday.size() < maxSize)
            thursday.add(null);
        while(friday.size() < maxSize)
            friday.add(null);
        while(saturday.size() < maxSize)
            saturday.add(null);
        while(sunday.size() < maxSize)
            sunday.add(null);
    }

    public static List<RecordHelper> getMonday() {
        return createList(monday);
    }

    public static List<RecordHelper> getTuesday() {
        return createList(tuesday);
    }

    public static List<RecordHelper> getWednesday() {
        return createList(wednesday);
    }

    public static List<RecordHelper> getThursday() {
        return createList(thursday);
    }

    public static List<RecordHelper> getFriday() {
        return createList(friday);
    }

    public static List<RecordHelper> getSaturday() {
        return createList(saturday);
    }

    public static List<RecordHelper> getSunday() {
        return createList(sunday);
    }

    private static List<RecordHelper> createList(List<Record> recordsOfDay){
        List<RecordHelper> items = new ArrayList<>();
        RecordHelper recordHelper;
        String teacher, time, type, subjectName;
        for(Record record : recordsOfDay){
            if(record != null) {
                teacher = record.getSubject().getTeacher().getTeacherName();
                time = record.getTime() + " - " + record.getEndTime();
                type = record.getSubject().getType().getName();
                subjectName = record.getSubject().getSubjectName();
                recordHelper = new RecordHelper(record.getDay(), teacher, time, type, subjectName);
                items.add(recordHelper);
            }
            else
                items.add(null);
        }

        return items;
    }
}
