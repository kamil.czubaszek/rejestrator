package pl.kamilczubaszek.helpers;

public class RecordHelper {
    private Long day;
    private String teacher;
    private String time;
    private String type;
    private String subjectName;

    public RecordHelper(Long day, String teacher, String time, String type, String subjectName) {
        this.day = day;
        this.teacher = teacher;
        this.time = time;
        this.type = type;
        this.subjectName = subjectName;
    }

    public Long getDay() {
        return day;
    }

    public String getTeacher() {
        return teacher;
    }

    public String getTime() {
        return time;
    }

    public String getType() {
        return type;
    }

    public String getSubjectName(){
        return subjectName;
    }
}
