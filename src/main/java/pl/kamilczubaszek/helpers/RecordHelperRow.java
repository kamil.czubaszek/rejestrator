package pl.kamilczubaszek.helpers;

import pl.kamilczubaszek.model.lessons.Plan;

public class RecordHelperRow {
    private RecordHelper monday;
    private RecordHelper tuesday;
    private RecordHelper wednesday;
    private RecordHelper thursday;
    private RecordHelper friday;
    private RecordHelper saturday;
    private RecordHelper sunday;

    public void setMonday(RecordHelper monday) {
        this.monday = monday;
    }

    public void setTuesday(RecordHelper tuesday) {
        this.tuesday = tuesday;
    }

    public void setWednesday(RecordHelper wednesday) {
        this.wednesday = wednesday;
    }

    public void setThursday(RecordHelper thursday) {
        this.thursday = thursday;
    }

    public void setFriday(RecordHelper friday) {
        this.friday = friday;
    }

    public void setSaturday(RecordHelper saturday) {
        this.saturday = saturday;
    }

    public void setSunday(RecordHelper sunday) {
        this.sunday = sunday;
    }

    public RecordHelper getMonday() {
        return monday;
    }

    public RecordHelper getTuesday() {
        return tuesday;
    }

    public RecordHelper getWednesday() {
        return wednesday;
    }

    public RecordHelper getThursday() {
        return thursday;
    }

    public RecordHelper getFriday() {
        return friday;
    }

    public RecordHelper getSaturday() {
        return saturday;
    }

    public RecordHelper getSunday() {
        return sunday;
    }
}
