package pl.kamilczubaszek.helpers;

import javax.servlet.http.HttpServletRequest;

public final class URLHelper {
    public static String getBaseURLPath(HttpServletRequest request) {
        return request.getScheme() + "://" + request.getServerName();
    }
}
