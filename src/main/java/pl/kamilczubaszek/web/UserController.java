package pl.kamilczubaszek.web;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import pl.kamilczubaszek.helpers.URLHelper;
import pl.kamilczubaszek.model.account.User;
import pl.kamilczubaszek.model.account.UserEdit;
import pl.kamilczubaszek.model.account.UserEditMail;
import pl.kamilczubaszek.registration.OnRegistrationCompleteEvent;
import pl.kamilczubaszek.service.SecurityService;
import pl.kamilczubaszek.service.UserService;
import pl.kamilczubaszek.validator.EditValidator;
import pl.kamilczubaszek.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Locale;

@Controller
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private SecurityService securityService;
    @Autowired
    private UserValidator userValidator;
    @Autowired
    private EditValidator editValidator;
    @Autowired
    ApplicationEventPublisher eventPublisher;
    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("userForm", new User());

        return "account/registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, HttpServletRequest request, Errors error) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "account/registration";
        }
        userService.save(userForm);
        try {
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent
                    (userForm, request.getLocale(), URLHelper.getBaseURLPath(request)));
        } catch (Exception me) {
            error.rejectValue("otherErrors", "email.notsend");
            return "redirect:/welcome";
        }

        return "redirect:/welcome";
    }

    @RequestMapping(value = "/registrationConfirm", method = RequestMethod.GET)
    public String confirmRegistration(final Locale locale, final Model model, @RequestParam("token") final String token){
        final String result = userService.validateVerificationToken(token);
        if (result.equals("valid")) {
            //final User user = userService.getUser(token);
            model.addAttribute("message", messageSource.getMessage("message.accountVerified", null, locale));
            return "redirect:/login/form?lang=" + locale.getLanguage();
        }

        model.addAttribute("message", messageSource.getMessage("auth.message." + result, null, locale));
        model.addAttribute("expired", "expired".equals(result));
        model.addAttribute("token", token);
        return "redirect:/badUser.html?lang=" + locale.getLanguage();
    }

    @RequestMapping(value = "/change-password", method = RequestMethod.GET)
    public String editAccount(Model model) {
        model.addAttribute("editForm", new UserEdit());
        return "account/edit-password-form";
    }

    @RequestMapping(value = "/change-password", method = RequestMethod.POST)
    public String editAccount(@ModelAttribute("editForm") @Valid UserEdit userForm, BindingResult bindingResult, Model model) {
        User user = userService.getAuthUser();
        editValidator.validatePassword(user.getPassword(),userForm.getActualPassword(), bindingResult);
        editValidator.validate(userForm,bindingResult);

        if (bindingResult.hasErrors()) {
            return "account/edit-password-form";
        }

        Long userId = user.getId();
        userService.changePassword(userForm,userId);

        return "redirect:/logout";
    }

    @RequestMapping(value = "/login/form", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", messageSource.getMessage("message.login.invalid", null, Locale.getDefault()));

        if (logout != null)
            model.addAttribute("message", messageSource.getMessage("message.logout.success", null, Locale.getDefault()));
        return "account/login";
    }

    @RequestMapping(value = {"/welcome"}, method = RequestMethod.GET)
    public String welcome() {
        return "welcome";
    }

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String homePage() {
        return "home-page";
    }

    @GetMapping(value = "/accessDenied")
    public String accessDenied(){
        return "/errors/forbidden-403";
    }

    @GetMapping(value="/logout")
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/";
    }

    @GetMapping(value = "/change-mail")
    public String changeMail(Model model) {
        User user = userService.getAuthUser();
        UserEditMail editModel = new UserEditMail();
        editModel.setActualMail(user.getEmail());
        model.addAttribute("editForm", editModel);
        return "account/edit-mail-form";
    }

    @PostMapping(value = "/change-mail")
    public String changeMail(@ModelAttribute("editForm") @Valid UserEditMail editModel, BindingResult bindingResult, Model model) {
        User user = userService.getAuthUser();
        editValidator.validatePassword(user.getPassword(),editModel.getActualPassword(), bindingResult);
        if (bindingResult.hasErrors()) {
            editModel.setActualMail(user.getEmail());
            model.addAttribute("editForm", editModel);
            return "account/edit-mail-form";
        }
        userService.changeMail(editModel,user.getId());
        user.setEmail(editModel.getNewMail());
        return "redirect:/welcome";
    }
}
