package pl.kamilczubaszek.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import pl.kamilczubaszek.addons.Days;
import pl.kamilczubaszek.config.CustomAccessDeniedHandler;
import pl.kamilczubaszek.model.account.User;
import pl.kamilczubaszek.model.admissions.Admission;
import pl.kamilczubaszek.model.admissions.Record;
import pl.kamilczubaszek.model.admissions.helpers.JoinAdmissionHelper;
import pl.kamilczubaszek.model.lessons.Plan;
import pl.kamilczubaszek.model.lessons.Subject;
import pl.kamilczubaszek.service.*;
import pl.kamilczubaszek.validator.RecordValidator;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping(value = "/admission")
public class AdmissionController {
    @Autowired
    private RecordService recordService;

    @Autowired
    private AdmissionService admissionService;

    @Autowired
    UserService userService;

    @Autowired
    RecordValidator recordValidator;

    @Autowired
    SubjectService subjectService;

    @Autowired
    PlanService planService;

    @InitBinder
    public void initBinder(WebDataBinder binder){
        binder.registerCustomEditor(Date.class,
                new CustomDateEditor(new SimpleDateFormat("dd-mm-yyyy"), true, 10));
    }

    @GetMapping(value="/add-admission")
    public String addAdmission(Model model) {
        User user = userService.findById(userService.getAuthUser().getId());
        model.addAttribute("admissionForm", new Admission());
        model.addAttribute("plans",user.getPlans());
        return "admissions/add-admission-form";
    }

    @PostMapping(value="/add-admission")
    public String addAdmission(@ModelAttribute("admissionForm") @Valid Admission admission, BindingResult bindingResult, Model model) throws ParseException {
        if (bindingResult.hasErrors()) {
            User user = userService.findById(userService.getAuthUser().getId());
            model.addAttribute("plans",user.getPlans());
            return "admissions/add-admission-form";
        }

        admission.setUser(userService.getAuthUser());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date parsed = format.parse(admission.getStartDateTemp());
        admission.setStartDate(parsed);
        Plan plan = planService.getPlanById(admission.getPlanID());
        admission.setPlan(plan);
        admissionService.save(admission);
        return "redirect:/admission/my-admissions";
    }

    @GetMapping("/join-admission")
    public String joinAdmission(Model model, HttpSession session) {
        model.addAttribute("joinForm", new JoinAdmissionHelper());
        return "admissions/join-admission-form";
    }

    @PostMapping(value="/join-admission")
    public String joinAdmission(@ModelAttribute("joinForm") @Valid JoinAdmissionHelper joinAdmissionHelper, BindingResult bindingResult, Errors errors) throws ParseException {
        if (bindingResult.hasErrors()) {
            return "admissions/join-admission-form";
        }

        if(admissionService.joinToAdmission(joinAdmissionHelper,errors))
            return "redirect:/admission/my-admissions";
        else
            return "admissions/join-admission-form";
    }

    @GetMapping(value="/my-admissions")
    public String MyAdmissions(Model model) {
        User user = userService.findById(userService.getAuthUser().getId());
        model.addAttribute("admissions", user.getAdmissions());
        model.addAttribute("otherAdmissions", user.getOtherAdmissions());
        return "admissions/my-admissions";
    }
}
