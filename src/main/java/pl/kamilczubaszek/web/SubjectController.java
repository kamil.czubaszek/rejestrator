package pl.kamilczubaszek.web;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.kamilczubaszek.addons.Days;
import pl.kamilczubaszek.model.account.User;
import pl.kamilczubaszek.model.admissions.Record;
import pl.kamilczubaszek.model.lessons.Plan;
import pl.kamilczubaszek.model.lessons.Subject;
import pl.kamilczubaszek.model.lessons.Teacher;
import pl.kamilczubaszek.model.lessons.Type;
import pl.kamilczubaszek.service.*;
import pl.kamilczubaszek.tools.generator.ReportExporter;
import pl.kamilczubaszek.tools.generator.ReportFiller;
import pl.kamilczubaszek.validator.RecordValidator;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletResponse;
import pl.kamilczubaszek.tools.generator.PlanGenerator;

@Controller
@RequestMapping(value = "/subject")
public class SubjectController {
    @Autowired
    private LessonsService lessonsService;
    @Autowired
    private TeacherService teacherService;
    @Autowired
    private SubjectService subjectService;
    @Autowired
    private UserService userService;
    @Autowired
    private PlanService planService;
    @Autowired
    private RecordValidator recordValidator;
    @Autowired
    private RecordService recordService;
    @Autowired
    private PlanGenerator planGenerator;
    @Autowired
    private ReportFiller reportFiller;
    @Autowired
    private ReportExporter reportExporter;
    @GetMapping(value="/add-subject")
    public String addSubject(Model model) {
        model.addAttribute("subjectForm", new Subject());
        model.addAttribute("types", lessonsService.getAllTypes());
        model.addAttribute("teachers", teacherService.getAllTeachers());
        return "lessons/add-subject-form";
    }

    @PostMapping(value="/add-subject")
    public String addSubject(@ModelAttribute("subjectForm") @Valid Subject subject, BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()) {
            model.addAttribute("types", lessonsService.getAllTypes());
            model.addAttribute("teachers", teacherService.getAllTeachers());
            return "lessons/add-subject-form";
        }

        Teacher teacher = teacherService.getTeacherById(subject.getTeacherId());
        Type type = lessonsService.getTypeById(subject.getTypeId());
        subject.setTeacher(teacher);
        subject.setType(type);
        subject.setUser(userService.getAuthUser());
        subjectService.save(subject);
        return "redirect:/subject/show-subjects";
    }

    @GetMapping(value="/edit-subject/{subjectID}")
    public String editSubject(Model model, @PathVariable("subjectID") long subjectID, HttpSession session) {
        Subject editedSubject = subjectService.getSubjectById(subjectID);
        model.addAttribute("subjectModel", editedSubject);
        model.addAttribute("types", lessonsService.getAllTypes());
        model.addAttribute("teachers", teacherService.getAllTeachers());
        session.setAttribute("editedSubject", editedSubject);
        return "lessons/edit-subject-form";
    }

    @PostMapping(value="/edit-subject-success")
    public String editSubjectSuccess(@ModelAttribute("subjectModel") @Valid Subject subject, BindingResult bindingResult, Model model, HttpSession session){
        if (bindingResult.hasErrors()) {
            model.addAttribute("types", lessonsService.getAllTypes());
            model.addAttribute("teachers", teacherService.getAllTeachers());
            return "lessons/edit-subject-form";
        }

        Teacher teacher = teacherService.getTeacherById(subject.getTeacherId());
        Type type = lessonsService.getTypeById(subject.getTypeId());
        Subject editedSubject = (Subject) session.getAttribute("editedSubject");
        editedSubject.setTeacher(teacher);
        editedSubject.setType(type);
        subjectService.save(editedSubject);
        return "redirect:/subject/show-subjects";
    }

    @GetMapping(value="/delete-subject/{subjectID}")
    public String deleteSubject(@PathVariable("subjectID") long subjectID) {
        Subject deletedSubject = subjectService.getSubjectById(subjectID);
        subjectService.delete(deletedSubject);
        return "redirect:/subject/show-subjects";
    }

    @GetMapping(value="/add-type")
    public String addType(Model model) {
        model.addAttribute("typeForm", new Type());
        return "lessons/add-type-form";
    }

    @PostMapping(value="/add-type")
    public String addType(@ModelAttribute("typeForm") @Valid Type type, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "lessons/add-type-form";
        }
        type.setUser(userService.getAuthUser());
        lessonsService.save(type);
        return "redirect:/subject/show-types";
    }

    @GetMapping(value="/edit-type/{typeID}")
    public String editType(@PathVariable("typeID") long typeID,Model model, HttpSession session) {
        Type editedType = lessonsService.getTypeById(typeID);
        model.addAttribute("typeModel", editedType);
        session.setAttribute("editedType", editedType);
        return "lessons/edit-type-form";
    }

    @PostMapping(value="/edit-type-success")
    public String editTypeSuccess(@ModelAttribute("typeModel") @Valid Type type, BindingResult bindingResult, HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "lessons/edit-type-form";
        }
        Type editedType = (Type) session.getAttribute("editedType");
        editedType.setName(type.getName());
        lessonsService.save(editedType);
        return "redirect:/subject/show-types";
    }

    @GetMapping(value="/delete-type/{typeID}")
    public String deleteType(@PathVariable("typeID") long typeID) {
        Type deletedType = lessonsService.getTypeById(typeID);
        lessonsService.delete(deletedType);
        return "redirect:/subject/show-types";
    }

    @GetMapping(value="/add-teacher")
    public String addTeacher(Model model) {
        model.addAttribute("teacherForm", new Teacher());
        return "lessons/add-teacher-form";
    }

    @PostMapping(value="/add-teacher")
    public String addTeacher(@ModelAttribute("teacherForm") @Valid Teacher teacher, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "lessons/add-teacher-form";
        }
        teacher.setUser(userService.getAuthUser());
        teacherService.save(teacher);
        return "redirect:/subject/show-teachers";
    }

    @GetMapping(value="/edit-teacher/{teacherID}")
    public String editTeacher(@PathVariable("teacherID") long teacherID ,Model model, HttpSession session) {
        Teacher editedTeacher = teacherService.getTeacherById(teacherID);
        model.addAttribute("teacherForm", editedTeacher);
        session.setAttribute("editedTeacher", editedTeacher);
        return "lessons/edit-teacher-form";
    }

    @PostMapping(value="/edit-teacher-success")
    public String editTeacher(@ModelAttribute("teacherForm") @Valid Teacher teacher, BindingResult bindingResult, HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "lessons/edit-teacher-form";
        }
        Teacher editedTeacher = (Teacher) session.getAttribute("editedTeacher");
        editedTeacher.setTeacherName(teacher.getTeacherName());
        teacherService.save(editedTeacher);
        return "redirect:/subject/show-teachers";
    }

    @GetMapping(value="/delete-teacher/{teacherID}")
    public String deleteTeacher(@PathVariable("teacherID") long teacherID) {
        Teacher deletedTeacher = teacherService.getTeacherById(teacherID);
        teacherService.delete(deletedTeacher);
        return "redirect:/subject/show-teachers";
    }

    @GetMapping(value="/show-subjects")
    public String showSubjects(Model model) {
        Set<Subject> subjects = userService.findById(userService.getAuthUser().getId()).getSubjects();
        model.addAttribute("subjects", subjects);
        return "lessons/show-subjects";
    }

    @GetMapping(value="/show-teachers")
    public String showTeachers(Model model) {
        Set<Teacher> teachers = userService.findById(userService.getAuthUser().getId()).getTeachers();
        model.addAttribute("teachers", teachers);
        return "lessons/show-teachers";
    }

    @GetMapping(value="/show-types")
    public String showTypes(Model model) {
        Set<Type> types = userService.findById(userService.getAuthUser().getId()).getTypes();
        model.addAttribute("types", types);
        return "lessons/show-types";
    }

    @GetMapping(value="/add-plan")
    public String addPlan(Model model) {
        model.addAttribute("plan", new Plan());
        return "lessons/add-plan-form";
    }

    @PostMapping(value="/add-plan")
    public String addPlan(@ModelAttribute("plan") @Valid Plan plan, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "lessons/add-plan-form";
        }
        plan.setUser(userService.getAuthUser());
        planService.save(plan);
        return "redirect:/subject/show-plans";
    }

    @GetMapping(value="/show-plans")
    public String showPlans(Model model) {
        User user = userService.findById(userService.getAuthUser().getId());
        Set<Plan> myPlans = user.getPlans();
        Set<Plan> otherPlans = user.getOtherPlans();
        model.addAttribute("plans", myPlans);
        model.addAttribute("oPlans", otherPlans);
        model.addAttribute("plan", new Plan());
        return "lessons/my-plans";
    }

    @GetMapping("/show-plan/{planID}")
    public String showAdmission(@PathVariable("planID") long id, Model model,HttpSession session){
        User user = userService.findById(userService.getAuthUser().getId());
        Plan plan = planService.getPlanById(id);
        model.addAttribute("records", plan.getRecords());
        model.addAttribute("id", id);
        model.addAttribute("days", Days.values());
        model.addAttribute("subjects",subjectService.getAllSubjects());
        model.addAttribute("userRecords",user.getRecords());
        session.setAttribute("plan",plan);

        if(user.getPlans().contains(plan)){
            model.addAttribute("recordForm",new Record());
            return "lessons/show-plan";
        }

        return "lessons/show-other-plan";
    }

    @GetMapping("/add-record/{planID}")
    public String addRecord(@PathVariable("planID") long id, Model model, HttpSession session) {
        User user = userService.findById(userService.getAuthUser().getId());
        Plan plan = planService.getPlanById(id);
        if(!user.getPlans().contains(plan)){
            throw new AccessDeniedException("");
        }
        model.addAttribute("recordForm", new Record());
        model.addAttribute("days", Days.values());
        model.addAttribute("subjects",subjectService.getAllSubjects());
        session.setAttribute("plan",plan);
        return "lessons/add-record-form";
    }

    @PostMapping("/add-record-success")
    public String addRecordSend(@ModelAttribute("recordForm") @Valid Record record,BindingResult bindingResult, HttpSession session, Model model) {
        recordValidator.validate(record,bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("days", Days.values());
            model.addAttribute("subjects",subjectService.getAllSubjects());
            return "lessons/add-record-form";
        }

        Plan plan = (Plan) session.getAttribute("plan");
        record.setPlan(plan);

        Subject subject = subjectService.getSubjectById(record.getSubjectId());
        record.setSubject(subject);
        recordService.save(record);
        return "redirect:/subject/show-plan/"+plan.getId().toString();
    }

    @GetMapping("/modify-record/{recordID}/{planID}")
    public String modifyRecord(@PathVariable("recordID") long id, @PathVariable("planID") long planId, Model model, HttpSession session) {
        User user = userService.findById(userService.getAuthUser().getId());
        Plan plan = planService.getPlanById(planId);
        Record record = recordService.getRecordById(id);
        if(!user.getPlans().contains(plan) || !plan.getRecords().contains(record)){
            throw new AccessDeniedException("");
        }

        model.addAttribute("recordForm",record);
        model.addAttribute("days", Days.values());
        model.addAttribute("subjects",subjectService.getAllSubjects());
        session.setAttribute("recordID",id);
        session.setAttribute("plan",plan);
        return "lessons/modify-record-form";
    }

    @PostMapping("/modify-record-success")
    public String modifyRecordSuccess(@ModelAttribute("recordForm") @Valid Record record, BindingResult bindingResult, HttpSession session, Model model) {
        recordValidator.validate(record,bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("days", Days.values());
            model.addAttribute("subjects",subjectService.getAllSubjects());
            model.addAttribute("recordForm",record);
            return "lessons/modify-record-form";
        }

        long recordID = (long) session.getAttribute("recordID");
        Subject subject = subjectService.getSubjectById(record.getSubjectId());
        Record updatedRecord = recordService.getRecordById(recordID);
        updatedRecord.setSubject(subject);
        updatedRecord.setDay(record.getDay());
        updatedRecord.setEndTime(record.getEndTime());
        updatedRecord.setTime(record.getTime());
        recordService.save(record);
        return "redirect:/subject/show-plan/"+updatedRecord.getPlan().getId().toString();
    }

    @GetMapping("/delete-record/{recordID}/{planID}")
    public String deleteRecord(@PathVariable("recordID") long id, @PathVariable("planID") long planId) {
        User user = userService.findById(userService.getAuthUser().getId());
        Plan plan = planService.getPlanById(planId);
        Record record = recordService.getRecordById(id);
        if(!user.getPlans().contains(plan) || !plan.getRecords().contains(record)){
            throw new AccessDeniedException("");
        }

        recordService.delete(record);
        return "redirect:/subject/show-plan/{planID}";
    }

    @GetMapping("/edit-plan/{planID}")
    public String editPlan(@PathVariable("planID") long planID,  Model model, HttpSession session) {
        User user = userService.findById(userService.getAuthUser().getId());
        Plan plan = planService.getPlanById(planID);
        if(!user.getPlans().contains(plan)){
            throw new AccessDeniedException("");
        }

        model.addAttribute("plan",plan);
        session.setAttribute("planID",planID);
        return "lessons/edit-plan-form";
    }

    @PostMapping(value="/edit-plan-success")
    public String editPlanSuccess(@ModelAttribute("plan") @Valid Plan plan, BindingResult bindingResult, HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "lessons/edit-plan-form";
        }
        long planID = (long) session.getAttribute("planID");
        Plan editedPlan = planService.getPlanById(planID);
        editedPlan.setName(plan.getName());
        planService.save(editedPlan);
        return "redirect:/subject/show-plans";
    }

    @GetMapping("/delete-plan/{planID}")
    public String deletePlan(@PathVariable("planID") long planID) {
        User user = userService.findById(userService.getAuthUser().getId());
        Plan plan = planService.getPlanById(planID);
        if(!user.getPlans().contains(plan)){
            throw new AccessDeniedException("");
        }

        planService.delete(plan,user);
        return "redirect:/subject/show-plans";
    }
    
    @GetMapping(value="/join/{recordID}/{planID}")
    public String join(@PathVariable("recordID") long recordId, @PathVariable("planID") long planId) {
        User user = userService.findById(userService.getAuthUser().getId());
        Plan plan = planService.getPlanById(planId);
        Record record = recordService.getRecordById(recordId);
        if(record.getBooked() < record.getAmount()){
            Set<Record> records = user.getRecords();
            if(user.getPlans().contains(plan) || user.getOtherPlans().contains(plan)){
                if(!records.contains(record)){
                    int booked = record.getBooked();
                    record.setBooked(++booked);
                    records.add(record);
                    user.setRecords(records);
                    userService.update(user);
                    recordService.save(record);
                }
            }
        }
        return "redirect:/subject/show-plan/"+planId;
    }
    
    @GetMapping(value="/leave/{recordID}/{planID}")
    public String leave(@PathVariable("recordID") long recordId, @PathVariable("planID") long planId) {
        User user = userService.findById(userService.getAuthUser().getId());
        Plan plan = planService.getPlanById(planId);
        Record record = recordService.getRecordById(recordId);
        Set<Record> records = user.getRecords();
        if(user.getPlans().contains(plan) || user.getOtherPlans().contains(plan)){
            if(records.contains(record)){
                int booked = record.getBooked();
                record.setBooked(--booked);
                records.remove(record);
                user.setRecords(records);
                userService.update(user);
                recordService.save(record);
            }
        }
        return "redirect:/subject/show-plan/"+planId;
    }
    
    @ResponseBody
    @GetMapping(value="/generate-txt/{planID}")
    public String generateTXT(@PathVariable("planID") long planId, HttpServletResponse response){
        Plan plan = planService.getPlanById(planId);
        return planGenerator.generateTXT(plan,response);
    }

    @ResponseBody
    @GetMapping(value="/generate-pdf/{planID}")
    public void generatePDF(@PathVariable("planID") long planId, HttpServletResponse response) throws JRException, IOException {
        Plan plan = planService.getPlanById(planId);
        planGenerator.generatePDF(plan,response);
    }
}
