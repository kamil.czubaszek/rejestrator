package pl.kamilczubaszek.addons;

public enum Days {
    MONDAY(1L,"Poniedziałek"),
    TUESDAY(2L,"Wtorek"),
    WEDNESDAY(3L,"Środa"),
    THURSDAY(4L,"Czwartek"),
    FRIDAY(5L,"Piątek"),
    SATURDAY(6L,"Sobota"),
    SUNDAY(7L,"Niedziela");

    private String name;
    private Long id;

    Days(Long id, String name){
        this.id = id;
        this.name = name;
    }

    public static Days fromInteger(int x) {
        switch(x) {
            case 1:
                return MONDAY;
            case 2:
                return TUESDAY;
            case 3:
                return WEDNESDAY;
            case 4:
                return THURSDAY;
            case 5:
                return FRIDAY;
            case 6:
                return SATURDAY;
            case 7:
                return SUNDAY;
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
