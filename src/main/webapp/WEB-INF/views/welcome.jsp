<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html>
<head>
    <title>Rejestrator - Strona główna</title>
    <%@ include file="templates/header.jsp" %>
</head>
<body>
<%@ include file="templates/menu.jsp" %>
<div class="container">
    <h2>Witaj ${pageContext.request.userPrincipal.name}</h2>

</div>
<%@ include file="templates/footer.jsp" %>
</body>
</html>
