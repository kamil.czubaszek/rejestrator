<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Kamil
  Date: 16.12.2017
  Time: 18:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8" %>
<html>
<head>
    <title>Lista moich zapisów</title>
    <%@ include file="../templates/header.jsp" %>
</head>
<body>
<%@ include file="../templates/menu.jsp" %>
<div class="container">
    <c:choose>
        <c:when test="${admissions.size()>0 || otherAdmissions.size() > 0}">
            <c:choose>
                <c:when test="${admissions.size()>0}">
                    <h2>Zapisy utworzone przez Ciebie</h2>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Nazwa zapisów</th>
                            <th>Plan</th>
                            <th>Aktywne</th>
                            <th>Data startu zapisów</th>
                            <th>Edytuj</th>
                            <th>Usuń</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="admission" items="${admissions}">
                            <tr>
                                <td>${admission.name}</td>
                                <td><a href="/subject/show-plan/${admission.plan}">Przejdź</a></td>
                                <c:choose>
                                    <c:when test="${admission.active == true}">
                                        <td>Tak</td>
                                    </c:when>
                                    <c:otherwise>
                                        <td>Nie</td>
                                    </c:otherwise>
                                </c:choose>
                                <td>${admission.startDate}</td>
                                <td><a href="/admission/edit-admission/${admission.id}"><span
                                        class="glyphicon glyphicon-edit"></span></a></td>
                                <td><a href="/admission/delete-admission/${admission.id}"
                                       onclick="return confirm('Czy masz pewność, że chcesz usunąć te zapisy?');"><span
                                        class="glyphicon glyphicon-remove"></span></a></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:when test="${otherAdmissions.size()>0}">
                    <h2>Zapisy, do których dołączono</h2>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Nazwa zapisów</th>
                            <th>Plan</th>
                            <th>Aktywne</th>
                            <th>Data startu zapisów</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="admission" items="${otherAdmissions}">
                            <tr>
                                <td>${admission.name}</td>
                                <td><a href="/subject/show-plan/${admission.plan}">Przejdź</a></td>
                                <c:choose>
                                    <c:when test="${admission.active == true}">
                                        <td>Tak</td>
                                    </c:when>
                                    <c:otherwise>
                                        <td>Nie</td>
                                    </c:otherwise>
                                </c:choose>
                                <td>${admission.startDate}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
            </c:choose>
        </c:when>
        <c:otherwise>
            <h2 align="center">Brak danych do wyświetlenia</h2>
            <p align="center">Dodaj pierwsze zapisy <a href="/admission/add-admission">tutaj</a>. </p>
        </c:otherwise>
    </c:choose>
</div>
<%@ include file="../templates/footer.jsp" %>
</body>
</html>
