<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Kamil
  Date: 17.12.2017
  Time: 14:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8"%>
<%@ page import="pl.kamilczubaszek.addons.Days" %>

<html>
<head>
    <title>Zapisy</title>
    <%@ include file="../templates/header.jsp" %>
</head>
<body>
<%@ include file="../templates/menu.jsp" %>
<table>
    <td>Nazwa przedmiotu</td>
    <td>&nbsp;</td>
    <td>Prowadzący</td>
    <td>&nbsp;</td>
    <td>Typ przedmiotu</td>
    <td>&nbsp;</td>
    <td>Dzień</td>
    <td>&nbsp;</td>
    <td>Początek zajęć</td>
    <td>&nbsp;</td>
    <td>Koniec zajęć</td>
    <td>&nbsp;</td>
    <c:forEach var="record" items="${records}">
        <s:eval expression="T(pl.kamilczubaszek.addons.Days).fromInteger(record.day).name" var="dayName" />
        <tr>
            <td>${record.subject.subjectName}</td>
            <td>&nbsp;</td>
            <td>${record.subject.teacher.teacherName}</td>
            <td>&nbsp;</td>
            <td>${record.subject.type.name}</td>
            <td>&nbsp;</td>
            <td>${dayName}</td>
            <td>&nbsp;</td>
            <td>${record.time}</td>
            <td>&nbsp;</td>
            <td>${record.endTime}</td>
            <td>&nbsp;</td>
        </tr>
    </c:forEach>
</table>
<%@ include file="../templates/footer.jsp" %>
</body>
</html>
