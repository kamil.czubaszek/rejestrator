<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Kamil
  Date: 09.12.2017
  Time: 13:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8"%>
<html>
<head>
    <title>Dołącz do istniejących zapisów</title>
    <%@ include file="../templates/header.jsp" %>
</head>
<body>
<%@ include file="../templates/menu.jsp" %>
<div class="container">
    <form:form method="POST" modelAttribute="joinForm" class="form-signin">
        <h2 class="form-signin-heading">Dodaj nowe zapisy</h2>

        <spring:bind path="id">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="number" path="id" class="form-control" placeholder="ID zapisów" />
                <form:errors path="id" />
            </div>
        </spring:bind>

        <spring:bind path="password">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="password" path="password" class="form-control" placeholder="Hasło do zapisów" />
                <form:errors path="password" />
            </div>
        </spring:bind>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Wyślij</button>
    </form:form>

</div>
<%@ include file="../templates/footer.jsp" %>
</body>
</html>
