<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Kamil
  Date: 09.12.2017
  Time: 13:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8"%>
<html>
<head>
    <title>Utwórz nowe zapisy</title>
    <%@ include file="../templates/header.jsp" %>
</head>
<body>
<%@ include file="../templates/menu.jsp" %>
<div class="container">
    <form:form method="POST" modelAttribute="admissionForm" class="form-signin">
        <h2 class="form-signin-heading">Dodaj nowe zapisy</h2>

        <spring:bind path="name">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="text" path="name" class="form-control" placeholder="Nazwa" />
                <form:errors path="name" />
            </div>
        </spring:bind>

        <spring:bind path="planID">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:select  path="planID" multiple="false" class="form-control">
                    <form:option value="NONE">Wybierz plan</form:option>
                    <c:forEach var="plan" items="${plans}">
                        <form:option value="${plan.id}"><c:out value="${plan.name}"/></form:option>
                    </c:forEach>
                </form:select>
            </div>
        </spring:bind>

        <spring:bind path="password">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="password" path="password" class="form-control" placeholder="Hasło do zapisów" />
                <form:errors path="password" />
            </div>
        </spring:bind>

        <spring:bind path="startDateTemp">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="date" path="startDateTemp" class="form-control" placeholder="Wybierz datę zapisów" />
                <form:errors path="startDateTemp" />
            </div>
        </spring:bind>

        <spring:bind path="startTime">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="time" path="startTime" class="form-control" placeholder="Wybierz godzinę"
                            autofocus="true" />
                <form:errors path="startTime" />
            </div>
        </spring:bind>

        <spring:bind path="active">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:select  path="active" multiple="false" class="form-control">
                    <form:option value="1">Zapisy aktywne</form:option>
                    <form:option value="0">Zapisy nieaktywne</form:option>
                </form:select>
            </div>
        </spring:bind>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Wyślij</button>
    </form:form>

</div>
<%@ include file="../templates/footer.jsp" %>
</body>
</html>
