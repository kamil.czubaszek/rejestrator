<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Kamil
  Date: 09.12.2017
  Time: 13:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8"%>
<html>
<head>
    <title>Utwórz nowy plan</title>
    <%@ include file="../templates/header.jsp" %>
</head>
<body>
<%@ include file="../templates/menu.jsp" %>
<div class="container">
    <form:form method="POST" modelAttribute="plan" class="form-signin">
        <h2 class="form-signin-heading">Dodaj plan</h2>

        <spring:bind path="name">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="text" path="name" class="form-control" placeholder="Nazwa planu" />
                <form:errors path="name" />
            </div>
        </spring:bind>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Utwórz</button>
    </form:form>

</div>
<%@ include file="../templates/footer.jsp" %>
</body>
</html>
