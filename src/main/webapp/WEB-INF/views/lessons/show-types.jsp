<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Kamil
  Date: 16.12.2017
  Time: 18:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8" %>
<html>
<head>
    <title>Lista typów zajęć</title>
    <%@ include file="../templates/header.jsp" %>
</head>
<body>
<%@ include file="../templates/menu.jsp" %>
<div class="container">
    <c:choose>
        <c:when test="${types.size()>0}">
            <button type="button" class="btn btn-primary" onclick="location.href='/subject/add-type'"
                    style="float: right">Dodaj typ zajęć
            </button>
            <h2>Lista dodanych typów zajęć</h2>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Nazwa</th>
                    <th>Edytuj</th>
                    <th>Usuń</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="type" items="${types}">
                    <tr>
                        <td>${type.name}</td>
                        <td><a href="/subject/edit-type/${type.id}"><span class="glyphicon glyphicon-edit"></span></a>
                        </td>
                        <td><a href="/subject/delete-type/${type.id}"><span
                                class="glyphicon glyphicon-remove"
                                onclick="return confirm('Czy na pewno chcesz usunąć ten typ zajęć?');"></span></a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:when>
        <c:otherwise>
            <h2 align="center">Brak danych do wyświetlenia</h2>
            <p align="center">Dodaj pierwszy typ zajęć <a href="/subject/add-type/">tutaj</a>. </p>
        </c:otherwise>
    </c:choose>
</div>
<%@ include file="../templates/footer.jsp" %>
</body>
</html>
