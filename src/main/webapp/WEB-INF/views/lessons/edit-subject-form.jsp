<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Kamil
  Date: 09.12.2017
  Time: 10:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edycja przedmiotu</title>
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
</head>
<body>
<%@ include file="../templates/menu.jsp" %>
<div class="container">
    <form:form method="POST" modelAttribute="subjectModel" class="form-signin" action="/subject/edit-subject-success">
        <h2 class="form-signin-heading">Edytuj przedmiot</h2>
        <spring:bind path="subjectName">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="text" path="subjectName" class="form-control" placeholder="Nazwa przedmiotu"
                            autofocus="true" />
                <form:errors path="subjectName" />
            </div>
        </spring:bind>

        <spring:bind path="teacherId">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:select  path="teacherId" multiple="false" class="form-control">
                    <form:option value="NONE">Wybierz prowadzącego</form:option>
                    <c:forEach var="teacherSubject" items="${teachers}">
                        <form:option value="${teacherSubject.id}"><c:out value="${teacherSubject.teacherName}"/></form:option>
                    </c:forEach>
                </form:select>
            </div>
        </spring:bind>

        <spring:bind path="typeId">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:select  path="typeId" multiple="false" class="form-control">
                    <form:option value="NONE">Wybierz typ przedmiotu</form:option>
                    <c:forEach var="typeSubject" items="${types}">
                        <form:option value="${typeSubject.id}"><c:out value="${typeSubject.name}"/></form:option>
                    </c:forEach>
                </form:select>
            </div>
        </spring:bind>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Zatwierdź</button>
    </form:form>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
