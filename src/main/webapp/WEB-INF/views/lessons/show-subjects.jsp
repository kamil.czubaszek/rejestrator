<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Kamil
  Date: 16.12.2017
  Time: 18:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8"%>
<html>
<head>
    <title>Lista przedmiotów</title>
    <%@ include file="../templates/header.jsp" %>
</head>
<body>
<%@ include file="../templates/menu.jsp" %>
<div class="container">
<c:choose>
    <c:when test="${subjects.size()>0}">
    <button type="button" class="btn btn-primary" onclick="location.href='/subject/add-subject'"
            style="float: right">Dodaj przedmiot
    </button>
    <h2>Lista dodanych przedmiotów</h2>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Nazwa</th>
            <th>Prowadzący</th>
            <th>Typ przedmiotu</th>
            <th>Edytuj</th>
            <th>Usuń</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="subject" items="${subjects}">
            <tr>
                <td>${subject.subjectName}</td>
                <td>${subject.teacher.teacherName}</td>
                <td>${subject.type.name}</td>
                <td><a href="/subject/edit-subject/${subject.id}"><span class="glyphicon glyphicon-edit"></span></a></td>
                <td><a href="/subject/delete-subject/${subject.id}"
                       onclick="return confirm('Czy masz pewność, że chcesz usunąć nauczyciela?');"><span class="glyphicon glyphicon-remove"></span></a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    </c:when>
    <c:otherwise>
        <h2 align="center">Brak danych do wyświetlenia</h2>
        <p align="center">Dodaj pierwszy przedmiot <a href="/subject/add-subject">tutaj</a>. </p>
    </c:otherwise>
</c:choose>
</div>
<%@ include file="../templates/footer.jsp" %>
</body>
</html>
