<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Kamil
  Date: 09.12.2017
  Time: 13:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8"%>
<html>
<head>
    <title>Zmień dane</title>
    <%@ include file="../templates/header.jsp" %>
</head>
<body>
<%@ include file="../templates/menu.jsp" %>
<div class="container">
    <form:form method="POST" modelAttribute="recordForm" class="form-signin" action="/subject/add-record-success" role="form">
        <h2 class="form-signin-heading">Dodaj przedmiot do planu</h2>

        <spring:bind path="subjectId">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:select  path="subjectId" multiple="false" class="form-control">
                    <form:option value="0">Wybierz przedmiot</form:option>
                    <c:forEach var="subject" items="${subjects}">
                        <form:option value="${subject.id}"><c:out value="${subject.subjectName}"/><c:out value=" (${subject.type.name} -" /><c:out value=" ${subject.teacher.teacherName})" /></form:option>
                    </c:forEach>
                </form:select>
                <form:errors path="subjectId" />
            </div>
        </spring:bind>

        <spring:bind path="day">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:select path="day" multiple="false" class="form-control">
                    <c:forEach var="dayTemp" items="${days}">
                        <form:option value="${dayTemp.id}"><c:out value="${dayTemp.name}"/></form:option>
                    </c:forEach>
                </form:select>
                <form:errors path="day" />
            </div>
        </spring:bind>

        <spring:bind path="time">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="time" path="time" class="form-control" placeholder="Wybierz początek zajęć" />
                <form:errors path="time" />
            </div>
        </spring:bind>

        <spring:bind path="endTime">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="time" path="endTime" class="form-control" placeholder="Wybierz koniec zajęć" />
                <form:errors path="endTime" />
            </div>
        </spring:bind>
        
        <spring:bind path="amount">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="number" path="amount" class="form-control" placeholder="Ilość miejsc" />
                <form:errors path="amount" />
            </div>
        </spring:bind>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Wyślij</button>
    </form:form>

</div>
<%@ include file="../templates/footer.jsp" %>
</body>
</html>
