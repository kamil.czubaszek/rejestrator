<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Kamil
  Date: 10.12.2017
  Time: 12:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8"%>
<html>
<head>
    <title>Dodawanie nowego prowadzącego zajęcia</title>
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
</head>
<body>
<%@ include file="../templates/menu.jsp" %>
<div class="container">
    <form:form method="POST" modelAttribute="teacherForm" class="form-signin">
        <h2 class="form-signin-heading">Dodaj prowadzącego</h2>
        <spring:bind path="teacherName">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="text" path="teacherName" class="form-control" placeholder="Imię i nazwisko"
                            autofocus="true" />
                <form:errors path="teacherName" />
            </div>
        </spring:bind>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Dodaj</button>
    </form:form>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
