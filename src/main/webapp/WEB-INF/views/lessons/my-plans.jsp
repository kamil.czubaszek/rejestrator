<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Kamil
  Date: 16.12.2017
  Time: 18:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<html>
<head>
    <title>Lista typów</title>
    <%@ include file="../templates/header.jsp" %>
</head>
<body>
<%@ include file="../templates/menu.jsp" %>
<div class="container">
    <button type="button" class="btn btn-primary" onclick="location.href='/subject/add-plan'" style="float: right">
        Utwórz nowy plan
    </button>
    <c:choose>
        <c:when test="${oPlans.size()>0 || plans.size() > 0}">
            <c:choose>
                <c:when test="${oPlans.size()>0}">
                    <h2>Plany z zapisów</h2>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Nazwa planu</th>
                            <th>Podgląd</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="oPlans" items="${oPlans}">
                            <tr>
                                <td>${oPlans.name}</td>
                                <td><a href="/subject/show-plan/${oPlans.id}"><span
                                        class="glyphicon glyphicon-eye-open"></span></a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:when test="${plans.size()>0}">
                    <h2 style="margin-top: 30px;">Plany utworzone przeze mnie</h2>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Nazwa planu</th>
                            <th>Podgląd</th>
                            <th>Edytuj</th>
                            <th>Usuń</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="plans" items="${plans}">
                            <tr>
                                <td>${plans.name}</td>
                                <td><a href="/subject/show-plan/${plans.id}"><span
                                        class="glyphicon glyphicon-eye-open"></span></a></td>
                                <td><a href="/subject/edit-plan/${plans.id}"><span
                                        class="glyphicon glyphicon-edit"></span></a></td>
                                <td><a href="/subject/delete-plan/${plans.id}"
                                       onclick="return confirm('Czy masz pewność, że chcesz usunąć ten plan?');"><span
                                        class="glyphicon glyphicon-remove"></span></a></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
            </c:choose>
        </c:when>
        <c:otherwise>
            <h2 align="center">Brak danych do wyświetlenia</h2>
            <p align="center">Dodaj pierwszy plan <a href="/subject/add-plan">tutaj</a>. </p>
        </c:otherwise>
    </c:choose>
</div>
<%@ include file="../templates/footer.jsp" %>
</body>
</html>
