<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Kamil
  Date: 16.12.2017
  Time: 18:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8" %>
<html>
<head>
    <title>Plan zajęć</title>
    <%@ include file="../templates/header.jsp" %>
    <link href="${contextPath}/resources/css/calendar.css" rel="stylesheet">
    <script src="${contextPath}/resources/js/calendar.js"></script>
</head>
<body>
<%@ include file="../templates/menu.jsp" %>
<div class="container">
    <div id="overlay" class="web_dialog_overlay"></div>
    <div id="dialog" class="web_dialog">
        <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0" class="title-popup-form">
            <tr>
                <td class="web_dialog_title" style="border-top-left-radius: 4px">Dodaj przedmiot</td>
                <td class="web_dialog_title align_right" style="border-top-right-radius: 4px">
                    <a href="#" id="btnClose"><span class="glyphicon glyphicon-remove"></span></a>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" style="padding-left: 15px;">
                    <form:form method="POST" modelAttribute="recordForm" class="form-signin"
                               action="/subject/add-record-success" role="form">
                        <h2 class="form-signin-heading">Wypełnij formularz</h2>

                        <spring:bind path="subjectId">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:select path="subjectId" multiple="false" class="form-control">
                                    <form:option value="0">Wybierz przedmiot</form:option>
                                    <c:forEach var="subject" items="${subjects}">
                                        <form:option value="${subject.id}"><c:out value="${subject.subjectName}"/><c:out
                                                value=" (${subject.type.name} -"/><c:out
                                                value=" ${subject.teacher.teacherName})"/></form:option>
                                    </c:forEach>
                                </form:select>
                                <form:errors path="subjectId"/>
                            </div>
                        </spring:bind>

                        <spring:bind path="day">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:select path="day" multiple="false" class="form-control">
                                    <form:option value="0" label="Wybierz dzień tygodnia"/>
                                    <c:forEach var="dayTemp" items="${days}">
                                        <form:option value="${dayTemp.id}"><c:out
                                                value="${dayTemp.name}"/></form:option>
                                    </c:forEach>
                                </form:select>
                                <form:errors path="day"/>
                            </div>
                        </spring:bind>

                        <spring:bind path="time">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:input type="time" path="time" class="form-control"
                                            placeholder="Wybierz początek zajęć"/>
                                <form:errors path="time"/>
                            </div>
                        </spring:bind>

                        <spring:bind path="endTime">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:input type="time" path="endTime" class="form-control"
                                            placeholder="Wybierz koniec zajêæ"/>
                                <form:errors path="endTime"/>
                            </div>
                        </spring:bind>

                        <spring:bind path="amount">
                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                <form:input type="number" path="amount" class="form-control" placeholder="Ilość miejsc" />
                                <form:errors path="amount" />
                            </div>
                        </spring:bind>

                        <button class="btn btn-lg btn-primary btn-block" type="submit">Wyślij</button>
                    </form:form>
                </td>
            </tr>
        </table>
    </div>
    <c:choose>
        <c:when test="${records.size()>0}">
            <div id='wrap'>
                <div id="calendar" class="fc fc-ltr">
                    <table class="fc-header" style="width:100%">
                        <tbody>
                        <tr>
                            <td class="fc-header-left">
                                <button type="button" class="btn btn-default" onclick="location.href='/subject/generate-txt/${plan.id}'">Generuj prostą listę</button>
                                <button type="button" class="btn btn-primary" onclick="location.href='/subject/generate-pdf/${plan.id}'">Zapisz plan w PDF</button>
                            </td>
                            <td class="fc-header-center">
                                <span class="fc-header-title"><h2>${plan.name}</h2></span>
                            </td>
                            <td class="fc-header-right">
                                <button type="button" class="btn btn-link" id="btnShowModal">Dodaj przedmiot do planu
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="fc-content" style="position: relative;">
                        <div class="fc-grid" style="position:relative" unselectable="on">
                            <table class="fc-border-separate" style="width:100%" cellspacing="0">
                                <thead>
                                <tr class="fc-first">
                                    <th class="fc-day-header fc-mon fc-widget-header" style="width: 128px;">
                                        Poniedziałek
                                    </th>
                                    <th class="fc-day-header fc-tue fc-widget-header" style="width: 128px;">Wtorek</th>
                                    <th class="fc-day-header fc-wed fc-widget-header" style="width: 128px;">Środa</th>
                                    <th class="fc-day-header fc-thu fc-widget-header" style="width: 128px;">Czwartek
                                    </th>
                                    <th class="fc-day-header fc-fri fc-widget-header" style="width: 128px;">Piątek</th>
                                    <th class="fc-day-header fc-sat fc-widget-header" style="width: 128px;">Sobota</th>
                                    <th class="fc-day-header fc-sun fc-widget-header" style="width: 128px;">Niedziela
                                    </th>
                                </tr>
                                </thead>

                                <tbody>
                                <tr>
                                    <td class="fc-day-content fc-mon fc-widget-content">
                                        <c:forEach var="record" items="${records}">
                                            <s:eval expression="T(pl.kamilczubaszek.addons.Days).fromInteger(record.day).name"
                                                    var="dayName"/>
                                            <c:set var="contains" value="false"/>
                                            <c:choose>
                                                <c:when test="${userRecords.contains(record)}"><c:set var="contains"
                                                                                                      value="true"/></c:when>
                                            </c:choose>
                                            <c:choose>
                                                <c:when test="${record.day==1}">
                                                    <div class="fc-event fc-event-hori">
                                                        <div class="fc-event-inner <c:if test="${contains eq 'true'}">joined</c:if>">
                                                            <div class="action-buttons">
                                                                <a
                                                                        href="/subject/delete-record/${record.id}/${id}"
                                                                        onclick="return confirm('Czy masz pewnoœæ, ¿e chcesz usun¹æ ten rekord?');"><span
                                                                        class="glyphicon glyphicon-remove" id="remove-record"></span></a>
                                                                <a href="/subject/modify-record/${record.id}/${id}"><span
                                                                        class="glyphicon glyphicon-edit" id="edit-record"></span></a>
                                                                <c:if test="${contains eq 'false'}">
                                                                    <a href="/subject/join/${record.id}/${id}"><span
                                                                            class="glyphicon glyphicon-floppy-saved"
                                                                            id="join-record"></span></a>
                                                                </c:if>
                                                                <c:if test="${contains eq 'true'}">
                                                                    <a href="/subject/leave/${record.id}/${id}"><span
                                                                            class="glyphicon glyphicon-floppy-remove"
                                                                            id="lost-record"></span></a>
                                                                </c:if>
                                                            </div>
                                                            <div class="data-box">
                                                                <span style="padding-right: 10px">${record.subject.subjectName}</span>
                                                                <p>${record.time} - ${record.endTime}</p>
                                                                <p>${record.subject.teacher.teacherName}</p>
                                                                <p>(${record.booked}/${record.amount})</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:when>
                                            </c:choose>
                                        </c:forEach>
                                    </td>
                                    <td class="fc-day-content fc-tue fc-widget-content">
                                        <c:forEach var="record" items="${records}">
                                            <s:eval expression="T(pl.kamilczubaszek.addons.Days).fromInteger(record.day).name"
                                                    var="dayName"/>
                                            <c:set var="contains" value="false"/>
                                            <c:choose>
                                                <c:when test="${userRecords.contains(record)}"><c:set var="contains"
                                                                                                      value="true"/></c:when>
                                            </c:choose>
                                            <c:choose>
                                                <c:when test="${record.day==2}">
                                                    <div class="fc-event fc-event-hori">
                                                        <div class="fc-event-inner <c:if test="${contains eq 'true'}">joined</c:if>">
                                                            <div class="action-buttons">
                                                                <a
                                                                        href="/subject/delete-record/${record.id}/${id}"
                                                                        onclick="return confirm('Czy masz pewnoœæ, ¿e chcesz usun¹æ ten rekord?');"><span
                                                                        class="glyphicon glyphicon-remove" id="remove-record"></span></a>
                                                                <a href="/subject/modify-record/${record.id}/${id}"><span
                                                                        class="glyphicon glyphicon-edit" id="edit-record"></span></a>
                                                                <c:if test="${contains eq 'false'}">
                                                                    <a href="/subject/join/${record.id}/${id}"><span
                                                                            class="glyphicon glyphicon-floppy-saved"
                                                                            id="join-record"></span></a>
                                                                </c:if>
                                                                <c:if test="${contains eq 'true'}">
                                                                    <a href="/subject/leave/${record.id}/${id}"><span
                                                                            class="glyphicon glyphicon-floppy-remove"
                                                                            id="lost-record"></span></a>
                                                                </c:if>
                                                            </div>
                                                            <div class="data-box">
                                                                <span style="padding-right: 10px">${record.subject.subjectName}</span>
                                                                <p>${record.time} - ${record.endTime}</p>
                                                                <p>${record.subject.teacher.teacherName}</p>
                                                                <p>(${record.booked}/${record.amount})</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:when>
                                            </c:choose>
                                        </c:forEach>
                                    </td>
                                    <td class="fc-day-content fc-wed fc-widget-content">
                                        <c:forEach var="record" items="${records}">
                                            <s:eval expression="T(pl.kamilczubaszek.addons.Days).fromInteger(record.day).name"
                                                    var="dayName"/>
                                            <c:set var="contains" value="false"/>
                                            <c:choose>
                                                <c:when test="${userRecords.contains(record)}"><c:set var="contains"
                                                                                                      value="true"/></c:when>
                                            </c:choose>
                                            <c:choose>
                                                <c:when test="${record.day==3}">
                                                    <div class="fc-event fc-event-hori">
                                                        <div class="fc-event-inner <c:if test="${contains eq 'true'}">joined</c:if>">
                                                            <div class="action-buttons">
                                                                <a
                                                                        href="/subject/delete-record/${record.id}/${id}"
                                                                        onclick="return confirm('Czy masz pewnoœæ, ¿e chcesz usun¹æ ten rekord?');"><span
                                                                        class="glyphicon glyphicon-remove" id="remove-record"></span></a>
                                                                <a href="/subject/modify-record/${record.id}/${id}"><span
                                                                        class="glyphicon glyphicon-edit" id="edit-record"></span></a>
                                                                <c:if test="${contains eq 'false'}">
                                                                    <a href="/subject/join/${record.id}/${id}"><span
                                                                            class="glyphicon glyphicon-floppy-saved"
                                                                            id="join-record"></span></a>
                                                                </c:if>
                                                                <c:if test="${contains eq 'true'}">
                                                                    <a href="/subject/leave/${record.id}/${id}"><span
                                                                            class="glyphicon glyphicon-floppy-remove"
                                                                            id="lost-record"></span></a>
                                                                </c:if>
                                                            </div>
                                                            <div class="data-box">
                                                                <span style="padding-right: 10px">${record.subject.subjectName}</span>
                                                                <p>${record.time} - ${record.endTime}</p>
                                                                <p>${record.subject.teacher.teacherName}</p>
                                                                <p>(${record.booked}/${record.amount})</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:when>
                                            </c:choose>
                                        </c:forEach>
                                    </td>
                                    <td class="fc-day-content fc-thu fc-widget-content">
                                        <c:forEach var="record" items="${records}">
                                            <s:eval expression="T(pl.kamilczubaszek.addons.Days).fromInteger(record.day).name"
                                                    var="dayName"/>
                                            <c:set var="contains" value="false"/>
                                            <c:choose>
                                                <c:when test="${userRecords.contains(record)}"><c:set var="contains"
                                                                                                      value="true"/></c:when>
                                            </c:choose>
                                            <c:choose>
                                                <c:when test="${record.day==4}">
                                                    <div class="fc-event fc-event-hori">
                                                        <div class="fc-event-inner <c:if test="${contains eq 'true'}">joined</c:if>">
                                                            <div class="action-buttons">
                                                                <a
                                                                        href="/subject/delete-record/${record.id}/${id}"
                                                                        onclick="return confirm('Czy masz pewnoœæ, ¿e chcesz usun¹æ ten rekord?');"><span
                                                                        class="glyphicon glyphicon-remove" id="remove-record"></span></a>
                                                                <a href="/subject/modify-record/${record.id}/${id}"><span
                                                                        class="glyphicon glyphicon-edit" id="edit-record"></span></a>
                                                                <c:if test="${contains eq 'false'}">
                                                                    <a href="/subject/join/${record.id}/${id}"><span
                                                                            class="glyphicon glyphicon-floppy-saved"
                                                                            id="join-record"></span></a>
                                                                </c:if>
                                                                <c:if test="${contains eq 'true'}">
                                                                    <a href="/subject/leave/${record.id}/${id}"><span
                                                                            class="glyphicon glyphicon-floppy-remove"
                                                                            id="lost-record"></span></a>
                                                                </c:if>
                                                            </div>
                                                            <div class="data-box">
                                                                <span style="padding-right: 10px">${record.subject.subjectName}</span>
                                                                <p>${record.time} - ${record.endTime}</p>
                                                                <p>${record.subject.teacher.teacherName}</p>
                                                                <p>(${record.booked}/${record.amount})</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:when>
                                            </c:choose>
                                        </c:forEach>
                                    </td>
                                    <td class="fc-day-content fc-fri fc-widget-content">
                                        <c:forEach var="record" items="${records}">
                                            <s:eval expression="T(pl.kamilczubaszek.addons.Days).fromInteger(record.day).name"
                                                    var="dayName"/>
                                            <c:set var="contains" value="false"/>
                                            <c:choose>
                                                <c:when test="${userRecords.contains(record)}"><c:set var="contains"
                                                                                                      value="true"/></c:when>
                                            </c:choose>
                                            <c:choose>
                                                <c:when test="${record.day==5}">
                                                    <div class="fc-event fc-event-hori">
                                                        <div class="fc-event-inner <c:if test="${contains eq 'true'}">joined</c:if>">
                                                            <div class="action-buttons">
                                                                <a
                                                                        href="/subject/delete-record/${record.id}/${id}"
                                                                        onclick="return confirm('Czy masz pewnoœæ, ¿e chcesz usun¹æ ten rekord?');"><span
                                                                        class="glyphicon glyphicon-remove" id="remove-record"></span></a>
                                                                <a href="/subject/modify-record/${record.id}/${id}"><span
                                                                        class="glyphicon glyphicon-edit" id="edit-record"></span></a>
                                                                <c:if test="${contains eq 'false'}">
                                                                    <a href="/subject/join/${record.id}/${id}"><span
                                                                            class="glyphicon glyphicon-floppy-saved"
                                                                            id="join-record"></span></a>
                                                                </c:if>
                                                                <c:if test="${contains eq 'true'}">
                                                                    <a href="/subject/leave/${record.id}/${id}"><span
                                                                            class="glyphicon glyphicon-floppy-remove"
                                                                            id="lost-record"></span></a>
                                                                </c:if>
                                                            </div>
                                                            <div class="data-box">
                                                                <span style="padding-right: 10px">${record.subject.subjectName}</span>
                                                                <p>${record.time} - ${record.endTime}</p>
                                                                <p>${record.subject.teacher.teacherName}</p>
                                                                <p>(${record.booked}/${record.amount})</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:when>
                                            </c:choose>
                                        </c:forEach>
                                    </td>
                                    <td class="fc-day-content fc-sat fc-widget-content">
                                        <c:forEach var="record" items="${records}">
                                            <s:eval expression="T(pl.kamilczubaszek.addons.Days).fromInteger(record.day).name"
                                                    var="dayName"/>
                                            <c:set var="contains" value="false"/>
                                            <c:choose>
                                                <c:when test="${userRecords.contains(record)}"><c:set var="contains"
                                                                                                      value="true"/></c:when>
                                            </c:choose>
                                            <c:choose>
                                                <c:when test="${record.day==6}">
                                                    <div class="fc-event fc-event-hori">
                                                        <div class="fc-event-inner <c:if test="${contains eq 'true'}">joined</c:if>">
                                                            <div class="action-buttons">
                                                                <a
                                                                        href="/subject/delete-record/${record.id}/${id}"
                                                                        onclick="return confirm('Czy masz pewnoœæ, ¿e chcesz usun¹æ ten rekord?');"><span
                                                                        class="glyphicon glyphicon-remove" id="remove-record"></span></a>
                                                                <a href="/subject/modify-record/${record.id}/${id}"><span
                                                                        class="glyphicon glyphicon-edit" id="edit-record"></span></a>
                                                                <c:if test="${contains eq 'false'}">
                                                                    <a href="/subject/join/${record.id}/${id}"><span
                                                                            class="glyphicon glyphicon-floppy-saved"
                                                                            id="join-record"></span></a>
                                                                </c:if>
                                                                <c:if test="${contains eq 'true'}">
                                                                    <a href="/subject/leave/${record.id}/${id}"><span
                                                                            class="glyphicon glyphicon-floppy-remove"
                                                                            id="lost-record"></span></a>
                                                                </c:if>
                                                            </div>
                                                            <div class="data-box">
                                                                <span style="padding-right: 10px">${record.subject.subjectName}</span>
                                                                <p>${record.time} - ${record.endTime}</p>
                                                                <p>${record.subject.teacher.teacherName}</p>
                                                                <p>(${record.booked}/${record.amount})</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:when>
                                            </c:choose>
                                        </c:forEach>
                                    </td>
                                    <td class="fc-day-content fc-sun fc-widget-content">
                                        <c:forEach var="record" items="${records}">
                                            <s:eval expression="T(pl.kamilczubaszek.addons.Days).fromInteger(record.day).name"
                                                    var="dayName"/>
                                            <c:set var="contains" value="false"/>
                                            <c:choose>
                                                <c:when test="${userRecords.contains(record)}"><c:set var="contains"
                                                                                                      value="true"/></c:when>
                                            </c:choose>
                                            <c:choose>
                                                <c:when test="${record.day==7}">
                                                    <div class="fc-event fc-event-hori">
                                                        <div class="fc-event-inner <c:if test="${contains eq 'true'}">joined</c:if>">
                                                            <div class="action-buttons">
                                                                <a
                                                                        href="/subject/delete-record/${record.id}/${id}"
                                                                        onclick="return confirm('Czy masz pewnoœæ, ¿e chcesz usun¹æ ten rekord?');"><span
                                                                        class="glyphicon glyphicon-remove" id="remove-record"></span></a>
                                                                <a href="/subject/modify-record/${record.id}/${id}"><span
                                                                        class="glyphicon glyphicon-edit" id="edit-record"></span></a>
                                                                <c:if test="${contains eq 'false'}">
                                                                    <a href="/subject/join/${record.id}/${id}"><span
                                                                            class="glyphicon glyphicon-floppy-saved"
                                                                            id="join-record"></span></a>
                                                                </c:if>
                                                                <c:if test="${contains eq 'true'}">
                                                                    <a href="/subject/leave/${record.id}/${id}"><span
                                                                            class="glyphicon glyphicon-floppy-remove"
                                                                            id="lost-record"></span></a>
                                                                </c:if>
                                                            </div>
                                                            <div class="data-box">
                                                                <span style="padding-right: 10px">${record.subject.subjectName}</span>
                                                                <p>${record.time} - ${record.endTime}</p>
                                                                <p>${record.subject.teacher.teacherName}</p>
                                                                <p>(${record.booked}/${record.amount})</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:when>
                                            </c:choose>
                                        </c:forEach>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </c:when>
        <c:otherwise>
            <h2 align="center">Brak danych do wyświetlenia</h2>
            <p align="center">Dodaj pierwszy przedmot do planu <a href="/subject/add-record/${id}">tutaj</a>. </p>
        </c:otherwise>
    </c:choose>
</div>
</div>
<%@ include file="../templates/footer.jsp" %>
</body>
</html>
