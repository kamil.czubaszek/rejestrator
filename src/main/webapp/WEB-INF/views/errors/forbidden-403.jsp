<%--
  Created by IntelliJ IDEA.
  User: Kamil
  Date: 27.12.2017
  Time: 20:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Dostęp zabroniony</title>
</head>
<body>
<%@ include file="../templates/menu.jsp" %>
    <h2>Niestety nie masz uprawnień, by oglądać tę stronę.</h2>
    <p><a href="/welcome">Powrót do strony głównej...</a></p>
</body>
</html>
