<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html>
<head>
    <title>Utwórz konto</title>
    <%@ include file="../templates/header.jsp" %>
</head>

<body>
<%@ include file="../templates/menu.jsp" %>
<div class="container">

    <form:form method="POST" modelAttribute="userForm" class="form-signin">
        <h2 class="form-signin-heading">Utwórz nowe konto</h2>
        
        <spring:bind path="firstName">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="text" path="firstName" class="form-control" placeholder="Imię"
                            autofocus="true" />
                <form:errors path="firstName" />
            </div>
        </spring:bind>
        
        <spring:bind path="lastName">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="text" path="lastName" class="form-control" placeholder="Nazwisko"
                            autofocus="true" />
                <form:errors path="lastName" />
            </div>
        </spring:bind>
        
        <spring:bind path="pesel">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="text" path="pesel" class="form-control" placeholder="PESEL"
                            autofocus="true" />
                <form:errors path="pesel" />
            </div>
        </spring:bind>
        
        <spring:bind path="username">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="text" path="username" class="form-control" placeholder="Nazwa użytkownika"
                            autofocus="true" />
                <form:errors path="username" />
            </div>
        </spring:bind>

        <spring:bind path="password">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="password" path="password" class="form-control" placeholder="Hasło" />
                <form:errors path="password" />
            </div>
        </spring:bind>

        <spring:bind path="passwordConfirm">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="password" path="passwordConfirm" class="form-control"
                            placeholder="Potwierdź hasło"/>
                <form:errors path="passwordConfirm"/>
            </div>
        </spring:bind>

        <spring:bind path="email">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="text" path="email" class="form-control" placeholder="Adres email"/>
                <form:errors path="email"/>
            </div>
        </spring:bind>

        <spring:bind path="emailConfirm">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="text" path="emailConfirm" class="form-control"
                            placeholder="Potwierdź adres email"/>
                <form:errors path="emailConfirm"/>
            </div>
        </spring:bind>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Wyślij</button>
    </form:form>

</div>
<%@ include file="../templates/footer.jsp" %>
</body>
</html>
