<%--
  Created by IntelliJ IDEA.
  User: Kamil
  Date: 29.11.2017
  Time: 21:30
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html>
<head>
    <title>Zmiana danych</title>
    <%@ include file="../templates/header.jsp" %>
</head>

<body>
<%@ include file="../templates/menu.jsp" %>
<div class="container">

    <form:form method="POST" modelAttribute="editForm" class="form-signin">
        <h2 class="form-signin-heading">Zmiana hasła</h2>

        <spring:bind path="actualPassword">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="password" path="actualPassword" class="form-control" placeholder="Aktualne hasło"></form:input>
                <form:errors path="actualPassword"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="newPassword">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="password" path="newPassword" class="form-control" placeholder="Nowe hasło"></form:input>
                <form:errors path="newPassword"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="passwordConfirm">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="password" path="passwordConfirm" class="form-control"
                            placeholder="Powtórz swoje hasło"></form:input>
                <form:errors path="passwordConfirm"></form:errors>
            </div>
        </spring:bind>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Zatwierdź</button>
    </form:form>

</div>
<%@ include file="../templates/footer.jsp" %>
</body>
</html>

