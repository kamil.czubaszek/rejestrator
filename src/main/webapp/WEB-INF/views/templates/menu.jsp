
<%--
  Created by IntelliJ IDEA.
  User: Kamil
  Date: 06.01.2018
  Time: 15:32
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<nav class="navbar navbar-inverse navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Rejestrator</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="/">Strona główna</a></li>
            <sec:authorize access="isAuthenticated()">
            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Zapisy <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="/admission/my-admissions">Wszystkie zapisy</a></li>
                    <li><a href="/admission/add-admission">Utwórz zapisy</a></li>
                    <li><a href="/admission/join-admission">Dołącz do zapisów</a></li>
                </ul>
            </li>
            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Plany zajęć <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="/subject/show-plans">Moje plany</a></li>
                    <li><a href="/subject/show-teachers">Prowadzący zajęcia</a></li>
                    <li><a href="/subject/show-types">Typy zajęć</a></li>
                    <li><a href="/subject/show-subjects">Przedmioty</a></li>
                </ul>
            </li>
            </sec:authorize>
            <li><a href="#">Funkcjonalność</a></li>
            <li><a href="#">API</a></li>
            <li><a href="#">Kontakt</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <sec:authorize access="isAnonymous()">
            <li><a href="/registration"><span class="glyphicon glyphicon-user"></span> Rejestracja</a></li>
            <li><a href="/login/form"><span class="glyphicon glyphicon-log-in"></span> Logowanie</a></li>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-user"></span> Konto użytkownika <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/change-password">Zmień hasło</a></li>
                        <li><a href="/change-mail">Zmień adres email</a></li>
                    </ul>
                </li>
                <li><a href="/logout"><span class="glyphicon glyphicon-log-out"></span> Wyloguj</a></li>
            </sec:authorize>
        </ul>
    </div>
</nav>
