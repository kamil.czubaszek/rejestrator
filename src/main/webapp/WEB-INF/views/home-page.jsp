<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html>
<head>
    <title>Rejestrator - Strona główna</title>
    <%@ include file="templates/header.jsp" %>
</head>
<body>
<%@ include file="templates/menu.jsp" %>
<div class="container">

    <h1 class="text-center">Witaj na stronie głównej Rejestratora!</h1>

    <div class="row text-center" id="home-data">
        <div class="col-sm-4">
            <h4 class="text-uppercase">Twórz zapisy na zajęcia</h4>
            <img src="${contextPath}/resources/img/lesson.png" alt="zapisy na zajęcia" class="img-responsive center-block" />
            <p class="text-center">Dzięki rejestratorowi możesz w łatwy i przyjemny sposób stworzyć zapisy na zajęcia dla swojego roku na uczelni. Po zapisach jest możliwość wygenerowania listy przedmiotów i osób, które są na nie zapisane.</p>
        </div>
        <div class="col-sm-4">
            <h4 class="text-uppercase">Generuj swój plan do PDF</h4>
            <img src="${contextPath}/resources/img/planning.png" alt="zapisy na zajęcia" class="img-responsive center-block" />
            <p class="text-center">Każdy uczestnik ma możliwość zapisania swojego planu do pliku PDF.</p>
        </div>
        <div class="col-sm-4">
            <h4 class="text-uppercase">Zamieniaj się przedmiotami</h4>
            <img src="${contextPath}/resources/img/switch.png" alt="zapisy na zajęcia" class="img-responsive center-block" />
            <p class="text-center">Jeśli wybrany przez Ciebie przedmiot Ci nie odpowiada, możesz założyć post z prośbą o zamianę.</p>
        </div>
    </div>

</div>
<%@ include file="templates/footer.jsp" %>
</body>
</html>
