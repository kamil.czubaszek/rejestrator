# Rejestrator (w trakcie rozwoju)
Rejestrator to aplikacja umożliwiająca tworzenie zapisów na zajęcia np. na studiach. Po utworzeniu konta, każdy użytkownik ma możliwość stworzenia nowych zapisów i planu. Każde zapisy są zabezpieczone hasłem i mają unikatowe ID, za pomocą którego można dołączyć do istniejących już zapisów. Najważniejsze funkcjonalności poniżej:  

  - Tworzenie zapisów i planu zajęć
  - Generowanie planu w dwóch formach: prostej - generowany jest plik *txt, rozbudowanej - generowany jest plik *pdf
  - System wymiany zajęciami w obrębie danych zapisów
  - Możliwość udostępnienia planu poprzez wygenerowanie odpowiedniego linku 
  - Zarządzanie swoimi planami
  - Generowanie listy osób zapisanych do poszczególnych zajęć
  - I inne...

### Stack technologiczny

W projekcie wykorzystywane są następujące technologie:

* [Bootstrap] - Frontend aplikacji
* [jQuery] - Skrypty aplikacji
* [JSP] - Tworzenie dynamicznych dokumentów WWW w formatach HTML
* [Spring] - Szkielet aplikacji w języku Java
* [Hibernate] - Zapewnia przede wszystkim translację danych pomiędzy relacyjną bazą danych a światem obiektowym
* [Java] - Backend
* [Jasper Reports] - Tworzenie dynamicznych wydruków
* [Intellij IDEA] - IDE do pisania kodu
* [Git] - Repozytorium
* [GitLab] - Zarządzanie projektami
* [Wildfly] - Serwer aplikacji
* [Maven] - Narzędzie automatyzujące budowę oprogramowania na platformę Java

### Instalacja
Aby uruchomić aplikację należy:
* Sklonować repozytorium
* Otworzyć repozytorium jako istniejacy projekt Maven w jakimś IDE np. Intellij, Eclipse itd.
* Dodać serwer aplikacji np. Wildfly czy Tomcat
* Zbudować i zdeployować aplikację

Licencja
----

MIT

### Demo
Aplikację można obejrzeć pod adresem: http://rejestrator-rejestrator.a3c1.starter-us-west-1.openshiftapps.com/
Niestety niektóre funkcje mogą nie działać, ponieważ maszyna na, której aplikacja jest zdeployowana jest dość słaba.

```sh
L: demo
P: demodemo
```

   [Bootstrap]: <https://getbootstrap.com/>
   [jQuery]: <http://jquery.com>
   [JSP]: <http://www.oracle.com/technetwork/java/index-jsp-138231.html>
   [Spring]: <https://spring.io/>
   [Hibernate]: <http://hibernate.org/>
   [Java]: <https://www.java.com/en/download/faq/whatis_java.xml>
   [Jasper Reports]: <https://community.jaspersoft.com/project/jasperreports-library>
   [Intellij IDEA]: <https://www.jetbrains.com/idea/>
   [Git]: <https://git-scm.com/>
   [GitLab]: <https://gitlab.com/>
   [Wildfly]: <http://wildfly.org/>
   [Maven]: <https://maven.apache.org/>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>
